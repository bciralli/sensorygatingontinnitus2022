#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@year: 2019
@license: GNU GPLv3 <https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022/raw/main/LICENSE>
@homepage: https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022
"""



#%% Prepare ====================================================================

from sciscripts.Exps import SoundAndLaserStimulation

Parameters = dict(
    ## === Animal === ##
    AnimalName          = 'Tinnitus_09',
    CageName            = 'A2',

    ## === Sound === ##
    SoundType           = 'Noise',
    StimType            = ['Sound'],
    NoiseFrequency      = [
        [8000, 10000],
        [9000, 11000],
        [10000, 12000],
        [12000, 14000],
        [14000, 16000]
    ],

    Intensities     = [80, 75, 70, 65, 60, 55, 50, 45],
    SoundPauseBeforePulseDur    = 0.004,
    SoundPulseDur               = 0.003,
    SoundPauseAfterPulseDur     = 0.093,
    SoundPulseNo                = 23**2,
    PauseBetweenIntensities     = 10,
    RecCh           = [1],
    StimCh          = 2,
    TTLCh           = 3,


    ## === Hardware === ##
    TTLAmpF         = 1.7,
    AnalogTTLs      = True,
    System          = 'Jack-IntelOut-Marantz-IntelIn',
    Setup           = 'UnitRec',
)


Stimulation, InfoFile = SoundAndLaserStimulation.Prepare(**Parameters)



#%% Run ========================================================================
SoundAndLaserStimulation.Play(Stimulation, InfoFile, ['Sound'], DV=None)



#%% EOF ========================================================================
