#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@year: 2019
@license: GNU GPLv3 <https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022/raw/main/LICENSE>
@homepage: https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022
"""



#%% Prepare ====================================================================

from sciscripts.Exps import SoundAndLaserStimulation

Parameters = dict(
    ## === Animal === ##
    AnimalName          = 'Tinnitus_11',
    CageName            = 'B2',


    ## === Sound === ##
    SoundType           = 'Noise',
    StimType            = ['Sound'],

    NoiseFrequency      = [[5000, 15000]],
    Intensities         = [85]*50,

    StimulationDelay            = 2,
    SoundPauseBeforePulseDur    = 0.01,
    SoundPulseDur               = 0.01,
    SoundPauseAfterPulseDur     = 0.48,
    SoundPulseNo                = 2,
    PauseBetweenIntensities     = [2,8],

    RecCh           = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    StimCh          = 17,
    TTLCh           = 18,


    ## === Hardware === ##
    TTLAmpF         = 1.7,
    AnalogTTLs      = True,
    System          = 'Jack-IntelOut-Marantz-IntelIn',
    Setup           = 'UnitRec',
)


Stimulation, InfoFile = SoundAndLaserStimulation.Prepare(**Parameters)


#%% Run ========================================================================
SoundAndLaserStimulation.Play(Stimulation, InfoFile, ['Sound', 'Cann', 'Nic'], DV=-1960)



#%% EOF ========================================================================
