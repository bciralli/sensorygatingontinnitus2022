#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@date: 20191218
@license: GNU GPLv3 <https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022/raw/main/LICENSE>
@homepage: https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022
"""



#%% Load dependencies ==========================================================
ScriptName = 'SensoryGatingOnTinnitus'

print(f'[{ScriptName}] Loading dependencies...')
import numpy as np, os
from glob import glob
from svgutils import compose as svgc
from svgutils import transform as svgt

from sciscripts.Analysis import Analysis
from sciscripts.Analysis import Stats
from sciscripts.Analysis.Plot import ABRs as ABRPlot, Plot
from sciscripts.IO import IO

plt = Plot.Return('plt')
ListedColormap = Plot.Return('ListedColormap')

DataPath = '/run/media/malfatti/Helvault/Ciralli/Documents/Data/SensoryGatingOnTinnitus'
AnalysisPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Analysis/SensoryGatingOnTinnitus'
FiguresPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Manuscripts/Ciralli_20200130/Figures'
SourcesPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Manuscripts/Ciralli_20200130/Sources'

GroupsOrder = ('Control', 'Tinnitus')
GroupsNames = ('Sham', 'Noise-induced tinnitus')
GroupsBreak = ('',' ')
TreatmentsOrder = ('NaCl', 'Nic', 'Cann', 'Cann+Nic')

Save = False
Margins = [50, 50]
A4Size = Plot.FigSizeA4.copy()
A4Size = [Size-Margins[S]*0.03937008 for S,Size in enumerate(A4Size)]
Ext = ['svg']
SPLA = {'size':14, 'va':'bottom', 'ha': 'left', 'weight':'bold'}
SigA = {'size':6}
LA = dict(handletextpad=0.05, borderpad=0.1, borderaxespad=0.1)

Markers = {
    'Control': '^',
    'Tinnitus': 's',
    'BeforeNE': 'o',
    'AfterNE': 'x'
}
Colors = {
    'NoGap': [103/255, 0, 103/255],
    'Gap': [235/255, 133/255, 24/255],
    'BeforeNE': [0, 0, 0],
    'AfterNE': [204/255, 0, 0],
    '1stClick': [0, 0, 0],
    '2ndClick': [204/255, 0, 0],
    'Control': [0, 120/255, 0],
    'Tinnitus': [0, 0, 120/255],
    'N40': [0, 1, 0],
    'P80': [0, 0, 1],
    'Arrows': 'c',
    'N40CMap': 'inferno',
    'P80CMap': 'inferno'
}
for E in ('Before','After'):
    Colors[f'{E}ANT'] = Colors[f'{E}NE']
    Markers[f'{E}ANT'] = Markers[f'{E}NE']

Colors = {K: np.array(V) for K,V in Colors.items()}


def AnovaStars(Anova, Ax, TicksDir='down', LineTextSpacing=1):
    ak = Stats.spk(Anova['PWCs']['Factor1'])
    AnovaInd = Anova['PWCs']['Factor1'][ak]['p'] < 0.05
    if True in AnovaInd:
        tr = Anova['PWCs']['Factor1'][ak]['Factor2'][AnovaInd]
        ps = Anova['PWCs']['Factor1'][ak]['p'][AnovaInd]
        g1 = Anova['PWCs']['Factor1'][ak]['group1'][AnovaInd]
        g2 = Anova['PWCs']['Factor1'][ak]['group2'][AnovaInd]

        for i,p in enumerate(ps):
            x = TreatmentsOrder.index(tr[i])*3
            x = [x, x+1]
            Y = max(Ax.get_ylim())
            p = '{:.1e}'.format(p)
            Plot.SignificanceBar(
                x, [Y*1.05]*2, p, Ax, TextArgs=SigA,
                TicksDir=TicksDir, LineTextSpacing=LineTextSpacing
            )

    ak = Stats.spk(Anova['PWCs']['Factor2'])
    AnovaInd = Anova['PWCs']['Factor2'][ak]['p'] < 0.05
    if True in AnovaInd:
        f1 = np.array([
            _ for a in [
                [b]*(
                    len(Anova['PWCs']['Factor2'][ak]['group1'])
                    //len(Anova['PWCs']['Factor2'][ak]['Factor1'])
                )
                for b in Anova['PWCs']['Factor2'][ak]['Factor1']
            ] for _ in a
        ])
        tr = f1[AnovaInd]
        ps = Anova['PWCs']['Factor2'][ak]['p'][AnovaInd]
        g1 = Anova['PWCs']['Factor2'][ak]['group1'][AnovaInd]
        g2 = Anova['PWCs']['Factor2'][ak]['group2'][AnovaInd]

        for i,p in enumerate(ps):
            x = [TreatmentsOrder.index(_)*3+1 for _ in [g1[i], g2[i]]]
            y = max(Ax.get_ylim())
            p = '{:.1e}'.format(p)
            Plot.SignificanceBar(
                x, [y*1.05]*2, p, Ax, TextArgs=SigA,
                TicksDir=TicksDir, LineTextSpacing=LineTextSpacing
            )

    for g in [1,2]:
        ak = Stats.spk(Anova['PWCs'][f'Factor{g}'])
        for f in zip(
                Anova['PWCs'][f'Factor{g}'][ak]['p'],
                Anova['PWCs'][f'Factor{g}'][ak]['group1'],
                Anova['PWCs'][f'Factor{g}'][ak]['group2']):
            print(f)

    return(Ax)


print(f'[{ScriptName}] Done.')



#%% Figure ABRs ================================================================
FigName = f'CiralliEtAl_FigABRs'
PlotType = 'Lines'
Traces = f'{AnalysisPath}/ABRsExample_9000-11000'
Pos = [1, 2, 4, 5, 7, 8, 10, 11, 13, 14]
Ticks = [1.5, 4.5, 7.5, 10.5, 13.5]
XLabels = ['8-10', '9-11', '10-12', '12-14', '14-16']
Treats = ['2d before', '2d after']


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.6]
Fig = plt.figure(figsize=FigSize, dpi=96, constrained_layout=True)
GS = Fig.add_gridspec(3,1,height_ratios=(0.2,0.4,0.4))
GSTop = GS[1].subgridspec(1,2)
GSBottom = GS[2].subgridspec(1,3)
AxImg = Fig.add_subplot(GS[0])
AxesTraces = np.array([
    Fig.add_subplot(GSTop[0]),
    Fig.add_subplot(GSTop[1]),
])
AxesQnt = np.array([
    Fig.add_subplot(GSBottom[0]),
    Fig.add_subplot(GSBottom[1]),
    Fig.add_subplot(GSBottom[2]),
])


# Load data
Scatter = {}
for Group in GroupsOrder:
    Scatter[Group] = glob(AnalysisPath+'/GroupData/ABRs/'+Group+'-Scatter/Threshold*.dat')
    Scatter[Group] = sorted(Scatter[Group], key=lambda x: int(x.split('/')[-1].split('_')[-1].split('.')[0]))
    Scatter[Group] = [IO.Bin.Read(Sc)[0] for Sc in Scatter[Group]]

Freqs = IO.Bin.Read(f'{AnalysisPath}/GroupData/ABRs/{Group}-Scatter/Freqs-Thresholds.dat')[0]
BF = ['-'.join([str(int(_)//1000) for _ in F.split('-')]) for F in Freqs]
ThrsCtr = Scatter['Control']
ThrsTinn = Scatter['Tinnitus']


# Add schematics text
TA = {'va':'bottom','ha':'center'}
AxImg.text(0, 0.05, 'Habituation', TA)
AxImg.text(1, 0.05, 'Tinnitus test', TA)
AxImg.text(2, 0.05, 'ABR', TA)
AxImg.text(3, 0.05, 'Noise-\nexposure', TA)
AxImg.text(4, 0.05, 'ABR', TA)
AxImg.text(5, 0.05, 'Tinnitus test', TA)
AxImg.text(6, 0.05, 'ERP', TA)
AxImg.set_xlabel('Experimental day')
AxImg.set_xlim(-0.5,6.5)
AxImg.set_xticks(range(7))
AxImg.set_xticklabels((1,7,8,10,12,13,'21-27'))
AxImg.set_ylim(0,1)
AxImg.set_yticks(())
AxImg.spines['left'].set_visible(False)
Plot.Set(Ax=AxImg)


# Plot traces
for G,Group in enumerate(GroupsOrder):
    ABR = IO.Bin.Read(f"{AnalysisPath}/GroupData/ABRs/Traces-{Group}-AfterANT")[0]
    ABRX = IO.Bin.Read(f"{AnalysisPath}/GroupData/ABRs/{Group}-X.dat")[0]
    ABRKeys = sorted(ABR.keys(), key=lambda x: int(x), reverse=True)
    Spaces = np.array(Plot.GetSpaces(np.array([np.nanmean(ABR[_], axis=1) for _ in ABRKeys]).T))*0.9
    Spaces[-4:] *= np.array((1.1,1.2,1.3,1.4))

    ABRYTicks = []
    for S,Space in enumerate(Spaces):
        Y = ABR[ABRKeys[S]]
        ABRYTicks.append(np.nanmean(Y+Space))

        AxesTraces[G] = Plot.MeanSEM(
            Y+Space, ABRX,
            LineArgs={'lw': 1, 'color': Colors[GroupsOrder[G]]},
            FillArgs={'color': Colors[GroupsOrder[G]]},
            Ax=AxesTraces[G]
        )

    AxesTraces[G].axvspan(
        ABRX[ABRX>= 0][0], ABRX[ABRX <= 3][-1],
        color='k', alpha=0.2, lw=0, label='Sound\nclick', zorder=-1
    )

    TracesTitle = Traces.split('/')[-1].split('_')[-1]
    TracesTitle = '-'.join([str(int(_)//1000) for _ in TracesTitle.split('-')])+'kHz'
    TracesTitle = f'{GroupsNames[G]} {TracesTitle}'
    XLim = np.round(ABRX[[0,-1]],1).astype(int)

    AxesTraces[G].set_xlim(XLim)
    AxesTraces[G].set_xticks(np.linspace(*XLim,6).astype(int))
    AxesTraces[G].set_xlabel('Time [ms]')
    AxesTraces[G].set_yticks(ABRYTicks)
    AxesTraces[G].set_yticklabels(ABRKeys)
    AxesTraces[G].set_ylabel('Intensity [dBSPL]')
    AxesTraces[G].set_title(TracesTitle)
    Plot.Set(Ax=AxesTraces[G])


# Plot quantifications
if PlotType == 'BoxPlot':
    C = [[Colors['Control']]*2, [Colors['Tinnitus']]*2]
    for G,Group in enumerate([ThrsCtr, ThrsTinn]):
        Plot.BoxPlots(Group, Pos, ['k', 'r']*(len(Group)//2), Ax=AxesQnt[G])

        AxArgs = {
            'xticks': Ticks,
            'xticklabels': XLabels,
            'ylim': [40,85],
            'yticks': range(40,81,10),
            'xlabel': 'Frequency [kHz]',
            'ylabel': 'Threshold [dBSPL]',
            'title': GroupsNames[G]
        }

        Plot.Set(Ax=AxesQnt[G], AxArgs=AxArgs)

    for G,Group in enumerate([ThrsCtr, ThrsTinn]):
        Shift = abs(np.diff(Group,axis=0))[::2,:]
        Plot.BoxPlots(Shift, Pos[G::2], [C[G][0]]*(len(Shift)), Ax=AxesQnt[-1])

        AxArgs = {
            'xticks': Ticks,
            'xticklabels': XLabels,
            'xlabel': 'Frequency [kHz]',
            'ylabel': 'Thr. shift [dBSPL]',
            'title': 'Thr. shift'
        }

        Plot.Set(Ax=AxesQnt[-1], AxArgs=AxArgs)

elif PlotType == 'Lines':
    C = [[Colors['Control']]*2, [Colors['Tinnitus']]*2]
    ms,mew,mai = 5, 1, 0.2
    for G,Group in enumerate([ThrsCtr, ThrsTinn]):
        g = np.array(Group).astype(float)
        g += np.random.uniform(-2,2,g.shape)

        g = np.array(Group)
        AxesQnt[G].plot(
            g[::2,:].mean(axis=1), color=C[G][0],
            marker=Markers['BeforeNE'], linestyle='--',
            markersize=ms, markeredgewidth=mew
        )
        AxesQnt[G].plot(
            g[1::2,:].mean(axis=1), color=C[G][1],
            marker=Markers['AfterNE'], linestyle='--',
            markersize=ms, markeredgewidth=mew
        )
        AxesQnt[G].legend(Treats, **LA)

        for a in range(g.shape[1]):
            AxesQnt[G].plot(
                g[::2,a], color=C[G][0],
                marker=Markers['BeforeNE'], linestyle='--', alpha=mai,
                markersize=ms, markeredgewidth=mew
            )
            AxesQnt[G].plot(
                g[1::2,a], color=C[G][1],
                marker=Markers['AfterNE'], linestyle='--', alpha=mai,
                markersize=ms, markeredgewidth=mew
            )

        g = np.array(Group)
        AxesQnt[G].plot(
            g[::2,:].mean(axis=1), color=C[G][0],
            marker=Markers['BeforeNE'], linestyle='--',
            markersize=ms, markeredgewidth=mew
        )
        AxesQnt[G].plot(
            g[1::2,:].mean(axis=1), color=C[G][1],
            marker=Markers['AfterNE'], linestyle='--',
            markersize=ms, markeredgewidth=mew
        )

        AxArgs = {
            'xticks': range(g.shape[0]//2),
            'xticklabels': XLabels,
            'ylim': [30,90],
            'xlabel': 'Frequency [kHz]',
            'ylabel': 'Threshold [dBSPL]',
            'title': GroupsNames[G]
        }
        AxesQnt[G].set_ylim((30,90))

        Plot.Set(Ax=AxesQnt[G], AxArgs=AxArgs)

    for G,Group in enumerate([ThrsCtr, ThrsTinn]):
        Shift = abs(np.diff(Group,axis=0))[::2,:]
        Plot.MeanSEM(Shift, range(len(Shift)), {'lw':1,'color':C[G][0]}, {'color':C[G][0]}, Ax=AxesQnt[-1])

        AxArgs = {
            'xticks': range(g.shape[0]//2),
            'xticklabels': XLabels,
            'ylim': [0,15],
            'yticks': np.linspace(0,15,4),
            'xlabel': 'Frequency [kHz]',
            'ylabel': 'Thr. shift [dBSPL]',
            'title': 'Thr. shift'
        }

        Plot.Set(Ax=AxesQnt[-1], AxArgs=AxArgs)


for Ax in AxesTraces: Ax.set_ylabel('Threshold [dBSPL]')

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add sublabels
Pos = np.array([
    _.get_position().corners()
    for _ in np.hstack((AxImg,AxesTraces,AxesQnt))
]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.01, Pos[2][0]-0.08, Pos[-1][0]-0.08]
Y = [0.96, Pos[1][1]+0.01, Pos[3][1]+0.02]
AxesPos = [
    [X[0], Y[0]],
    [X[0], Y[1]], [X[1], Y[1]],
    [X[0], Y[2]], [X[2], Y[2]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write
if Save:
    for E in Ext: Fig.savefig(f'{SourcesPath}/{FigName}-NoIMG.{E}', dpi=1200)
    FigSize_pt = Plot.InchToPoint(np.array(FigSize))
    svgc.Figure(str(FigSize_pt[0])+'pt', str(FigSize_pt[1])+'pt',
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-IMG.svg')),
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-NoIMG.svg'))
    ).save(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% Figure ABRs Wave amps and lats =============================================
FigName = f'CiralliEtAl_FigABRsWaves'
PlotType = 'Scatter'
Ticks = [1.5, 4.5, 7.5, 10.5, 13.5]
Treats = ['2d before', '2d after']
Sets = ('Amps', 'Latencies')
SetLabels = ('Amplitude [µV]', 'Latency [ms]')
Ws = (0,4)


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.4]
Fig = plt.figure(figsize=FigSize, dpi=96, constrained_layout=True)
Axes = np.array([
    Fig.add_subplot(2,1,1),
    Fig.add_subplot(2,1,2)

])


# Load data
WaveSet = {
    _: IO.Bin.Read(f"{AnalysisPath}/GroupData/ABRs/{_}-WaveAmpLat")[0]
    for _ in GroupsOrder
}
Freqs = sorted(
    np.unique([_ for Set in WaveSet.values() for _ in Set['Freqs']]),
    key=lambda x: int(x.split('-')[1])
)
Epochs = sorted(
    np.unique([_ for Set in WaveSet.values() for _ in Set['Epoch']]),
    reverse=True
)
BF = ['-'.join([str(int(_)//1000) for _ in F.split('-')]) for F in Freqs]

xFreq = np.array([Freq for Freq in Freqs for Group in GroupsOrder for Epoch in Epochs])
xGroup = np.array([Group for Freq in Freqs for Group in GroupsOrder for Epoch in Epochs])
xEpoch = np.array([Epoch for Freq in Freqs for Group in GroupsOrder for Epoch in Epochs])
xColor = np.array([Colors[Group] for Freq in Freqs for Group in GroupsOrder for Epoch in Epochs])
xMarker = np.array([Markers[Epoch] for Freq in Freqs for Group in GroupsOrder for Epoch in Epochs])

Pos = np.array([
    (F*(len(GroupsOrder)+len(Epochs)+3)) +
    (G*(len(Epochs)+1)) + E
    for F in range(len(Freqs))
    for G in range(len(GroupsOrder))
    for E in range(len(Epochs))
])
Ticks = Analysis.MovingAverage(Pos)[1::4]


# Plot quantifications
for S,Set in enumerate(Sets):
    Y = []
    for F,Freq in enumerate(Freqs):
        for G,Group in enumerate(GroupsOrder):
            iInt = WaveSet[Group]['Intensity']=='80'
            iFreq = WaveSet[Group]['Freqs']==Freq

            for E,Epoch in enumerate(Epochs):
                iEpoch = WaveSet[Group]['Epoch']==Epoch
                Y.append(WaveSet[Group][Set][:,iInt*iFreq*iEpoch][Ws[S],:])

    if PlotType == 'BoxPlot':
        Axes[S] = Plot.BoxPlots(
            Y, Pos, EdgeColors=xColor, FaceColors=xColor, Ax=Axes[S]
        )
    elif PlotType == 'Scatter':
        Axes[S] = Plot.ScatterMean(
            Y, Pos, Markers=xMarker, ColorsMarkers=xColor,
            ColorsLines=xColor, Ax=Axes[S]
        )

    Axes[S].set_xticks(Ticks)
    Axes[S].set_xticklabels(BF)
    Axes[S].set_ylabel(SetLabels[S])
    Axes[S].set_title(f'ABR Wave {Ws[S]+1}')

Axes[0].set_ylim((0,3000))
Axes[1].set_ylim((0,12))
Axes[1].set_xlabel('Frequency [kHz]')
for Ax in Axes: Plot.Set(Ax=Ax)

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Legend
LegLoc = 'upper left'
Legend = [
    Plot.GenLegendHandle(
        color=Colors['Control'], alpha=0.4,
        label=f'{GroupsNames[0]} bef.', marker=Markers['BeforeANT'], linestyle=''
    ),
    Plot.GenLegendHandle(
        color=Colors['Control'], alpha=0.4,
        label=f'{GroupsNames[0]} af.', marker=Markers['AfterANT'], linestyle=''
    ),
    Plot.GenLegendHandle(
        color=Colors['Tinnitus'], alpha=0.4,
        label=f'{GroupsNames[1]} bef.', marker=Markers['BeforeANT'], linestyle=''
    ),
    Plot.GenLegendHandle(
        color=Colors['Tinnitus'], alpha=0.4,
        label=f'{GroupsNames[1]} af.', marker=Markers['AfterANT'], linestyle=''
    ),
]
Axes[0].legend(loc=LegLoc, fontsize=7, handles=Legend, handlelength = 1.0, **LA)


# Add sublabels
Pos = np.array([_.get_position().corners() for _ in Axes]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.01]
Y = [0.96, Pos[1][1]+0.01]
AxesPos = [
    [X[0], Y[0]],
    [X[0], Y[1]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write
if Save:
    for E in Ext: Fig.savefig(f'{FiguresPath}/{FigName}.{E}')
plt.show()



#%% Figure GPIAS ===============================================================
FigName = f'CiralliEtAl_FigGPIAS'

# Set fig
FigSize = [A4Size[0], A4Size[1]*0.7]

Fig = plt.figure(figsize=FigSize, dpi=96, constrained_layout=True)
GS = Fig.add_gridspec(4,1,height_ratios=(0.5,1,1,1))
GSTraces = GS[1].subgridspec(2,3)
GSAllFreq = GS[2].subgridspec(1,2)
GSQnt = GS[3].subgridspec(1,3)

AxImg = Fig.add_subplot(GS[0])
AxesTraces = np.array([
    Fig.add_subplot(GSTraces[0,0]),
    Fig.add_subplot(GSTraces[1,0]),
    Fig.add_subplot(GSTraces[:,1]),
    Fig.add_subplot(GSTraces[:,2]),
])
AxesAllFreq = np.array([
    Fig.add_subplot(GSAllFreq[0]),
    Fig.add_subplot(GSAllFreq[1]),
])
AxesQnt = np.array([
    Fig.add_subplot(GSQnt[0]),
    Fig.add_subplot(GSQnt[1]),
    Fig.add_subplot(GSQnt[2]),
])


# Add schematics text
TA = {'va':'bottom','ha':'center'}
AxImg.text(0, 0.05, 'Habituation', TA)
AxImg.text(1, 0.05, 'Tinnitus test', TA)
AxImg.text(2, 0.05, 'ABR', TA)
AxImg.text(3, 0.05, 'Noise-\nexposure', TA)
AxImg.text(4, 0.05, 'ABR', TA)
AxImg.text(5, 0.05, 'Tinnitus test', TA)
AxImg.text(6, 0.05, 'ERP', TA)
AxImg.set_xlabel('Experimental day')
AxImg.set_xlim(-0.5,6.5)
AxImg.set_xticks(range(7))
AxImg.set_xticklabels((1,7,8,10,12,13,'21-27'))
AxImg.set_ylim(0,1)
AxImg.set_yticks(())
AxImg.spines['left'].set_visible(False)
Plot.Set(Ax=AxImg)


# GPIAS protocol
X = [0, 2.3, 2.3, 2.34, 2.34, 2.44, 2.44, 2.49, 2.49, 3]
Y1 = [60, 60, 60, 60, 60, 60, 105, 105, 60, 60]
Y2 = [60, 60, 0, 0, 60, 60, 105, 105, 60, 60]

AxesTraces[0].plot(X, Y1, 'k')
AxesTraces[1].plot(X, Y2, 'k')

AxArgs = {
    'title': 'Startle trial',
    'ylim': [0,105],
    'yticks': [0,60,105],
    'xticklabels': [],
    'xticks': [],
    'xlim': [0,3]
}
Plot.Set(Ax=AxesTraces[0], AxArgs=AxArgs)

AxArgs['title'] = 'Gap-Startle trial'
AxArgs['xlabel'] = 'Time [s]'
del(AxArgs['xticks'])
del(AxArgs['xticklabels'])
Plot.Set(Ax=AxesTraces[1], AxArgs=AxArgs)

AxesTraces[0].spines['bottom'].set_visible(False)


# Plot raw traces
Raw = IO.Bin.Read(f'{AnalysisPath}/GroupData/GPIAS/Tinnitus-RawTraces')[0]
Raw = {K: IO.Bin.DictToList(V) if K != 'X' else V for K,V in Raw.items()}
Raw = {K:V if K not in ['Animals', 'Freqs', 'Exps'] else np.array(V).ravel() for K,V in Raw.items()}
X = Raw['X']

for E, Exp in enumerate(['BeforeANT', 'AfterANT']):
    I = np.where(Raw['Exps'] == Exp)[0][0]
    Norm = np.max([abs(_) for _ in Raw['Traces'][I]])
    AxesTraces[E+2].axvspan(
        X[X >= 0][0], X[X >= 0.05*1000][0],
        color='k', alpha=0.4, lw=0, label='Sound pulse', zorder=0
    )
    AxesTraces[E+2].plot(
        X, Raw['Traces'][I][:,0]/Norm,
        color=Colors['NoGap'], label='Startle', lw=1.5
    )
    AxesTraces[E+2].fill_between(
        X, Raw['Traces'][I][:,0]/Norm, color=Colors['NoGap']
    )
    AxesTraces[E+2].plot(
        X, Raw['Traces'][I][:,1]/Norm,
        color=Colors['Gap'], label='Gap-Startle', lw=1.5
    )
    AxesTraces[E+2].fill_between(
        X, Raw['Traces'][I][:,1]/Norm, color=Colors['Gap']
    )
    AxesTraces[E+2].set_xlabel('Time [ms]')

AxesTraces[2].set_ylabel('Norm. resp. [a.u.]')
AxesTraces[2].set_title('Before exposure')
AxesTraces[3].set_title('After exposure')
AxesTraces[3].spines['left'].set_visible(False)
AxesTraces[3].set_yticks([])
AxesTraces[3].legend(bbox_to_anchor=(-0.65, -0.1, 1.0, 1.0), **LA)


# Plot all freqs
for G,Group in enumerate(GroupsOrder):
    All = IO.Bin.Read(f'{AnalysisPath}/GroupData/GPIAS/{Group}-AllFreqs.dat')[0]
    MAF = IO.Txt.Read(f'{AnalysisPath}/GroupData/GPIAS/{Group}-GPIASIndexes.dict')
    Freqs = MAF['Freqs']
    BF = ['-'.join([str(int(_)//1000) for _ in F.split('-')]) for F in Freqs]
    Positions = [_ for a in [[_*3, _*3+1] for _ in range(len(Freqs))] for _ in a]

    AxArgs = {
        'xticks': [sum([_*3, _*3+1])/2 for _ in range(len(Freqs))],
        'xticklabels': BF,
        'ylabel':'Startle suppr. [%]',
        'ylim': [-7, 107]
    }

    ColorsThis = [
        Colors['BeforeNE'],
        Colors['AfterNE'],
    ]

    ColorsThis *= len(Freqs)
    EMarkers = [Markers[_] for _ in ('BeforeNE','AfterNE')]*len(Freqs)

    for i in range(len(Freqs)):
        AxesAllFreq[G] = Plot.ScatterMean(All[i*2:i*2+2,:], Positions[i*2:i*2+2], Markers=EMarkers[i*2:i*2+2], ColorsMarkers=[Colors[GroupsOrder[G]]]*2, Paired=True, Ax=AxesAllFreq[G])

        for Line in AxesAllFreq[G].get_lines(): Line.set_fillstyle('none')

    AxesAllFreq[G].set_title(GroupsNames[G])
    AxesAllFreq[G].set_xlabel('Freq. [kHz]', labelpad=0)
    Plot.Set(Ax=AxesAllFreq[G], AxArgs=AxArgs)



# Plot quantifications
ControlMAF = IO.Txt.Read(f'{AnalysisPath}/GroupData/GPIAS/Control-GPIASIndexes.dict')
TinnitusMAF = IO.Txt.Read(f'{AnalysisPath}/GroupData/GPIAS/Tinnitus-GPIASIndexes.dict')
Freqs = ControlMAF['Freqs']

Results = [
    len([T for t,T in enumerate(TinnitusMAF['MAF']) if T == f])
    for f in sorted(
        np.unique([_ for _ in TinnitusMAF['MAF'] if _ is not None]),
        key=lambda x: int(x.split('-')[-1])
    )
]
BF = [
    '-'.join([str(int(_)//1000) for _ in f.split('-')]) for f in sorted(
        np.unique([_ for _ in TinnitusMAF['MAF'] if _ is not None]),
        key=lambda x: int(x.split('-')[-1])
    )
]


for E,Exp in enumerate([ControlMAF['Indexes'], TinnitusMAF['Indexes']]):
    Exp = [[-_ for _ in e] for e in Exp]
    EMarkers = [Markers[_] for _ in ('BeforeNE','AfterNE')]
    AxesQnt[E] = Plot.ScatterMean(
        Exp, Markers=EMarkers, ColorsMarkers=[Colors[GroupsOrder[E]]]*2,
        Paired=True, Ax=AxesQnt[E]
    )

    AxesQnt[E].set_xticklabels(['Before', 'After'])
    AxesQnt[E].get_lines()[0].set_fillstyle('none')
    AxesQnt[E].legend(
        np.array(AxesQnt[E].get_lines())[[0,4]],
        ('3d bef. NE', '3d af. NE'),
        handlelength = 1.2,
        loc = 'lower left',
        fontsize = 'x-small',
        **LA
    )

    p = IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/{GroupsOrder[E]}-GPIAS.dict')
    p = p['PWCs'][Stats.spk(p['PWCs'])]['p.adj'][0]
    if p < 0.05:
        p = '{:.1e}'.format(p)
        Plot.SignificanceBar(
            [0,1], [110,110], p,
            AxesQnt[E], TextArgs=SigA, TicksDir=None
        )


# Plot Histogram
MAFYTicks = list(range(len(BF)))

AxesQnt[2].barh(MAFYTicks, Results, color='k', edgecolor='k', alpha=0.6)
AxesQnt[2].set_yticks(MAFYTicks)
AxesQnt[2].set_ylabel('Most Aff. Freq. [kHz]', labelpad=0)
AxesQnt[2].set_xlabel('Number of animals')
AxesQnt[2].set_xticks(range(len(BF)+1))
AxesQnt[2].set_yticklabels(BF)
AxesQnt[2].set_xlim([-0.1, max(Results)])
AxesQnt[2].text(max(Results), AxesQnt[2].get_ylim()[1], 'n = 11', {'va':'top','ha':'right'})


# Set axes
for Ax in AxesTraces[:2]: Ax.set_ylabel(' ')
for A,Ax in enumerate((0,1)): AxesQnt[Ax].set_title(GroupsNames[A], pad=10)

for Ax in AxesQnt[:2]:
    Ax.set_xticks((0,1))
    Ax.set_yticks(np.linspace(0,100,5, dtype=int))
    Ax.set_ylabel('Startle suppr. [%]', labelpad=0)
    Ax.spines['left'].set_bounds(0,100)

for Ax in np.hstack((AxImg,AxesTraces,AxesQnt)): Plot.Set(Ax=Ax)

for Ax in AxesQnt: Ax.spines['bottom'].set_position(('outward',2))

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Fix positions
Pos = np.array([_.get_position().bounds for _ in AxesTraces]).T
Inc = 0.03
Pos[3,(0,1)] += Inc
Pos[1,0] -= Inc

for A,Ax in enumerate(AxesTraces): Ax.set_position(Pos[:,A])


# Text
Pos = np.array([_.get_position().bounds for _ in AxesTraces]).T
FA = {'rotation': 90, 'va': 'center', 'ha': 'center'}
Fig.text(Pos[0,1]-0.07, Pos[1,1]+(Pos[1,0]+Pos[3,0]-Pos[1,1])/2, 'Intensity [dB]', fontdict=FA)


# Add sublabels
Pos = np.array([
    _.get_position().corners()
    for _ in np.hstack((AxImg,AxesTraces,AxesAllFreq,AxesQnt))
]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0, Pos[3][0]-0.125, Pos[6][0]-0.125, Pos[9][0]-0.11]
Y = [0.96, Pos[1][1]+0.02, Pos[5][1]+0.02, Pos[7][1]+0.02]
AxesPos = [
    [X[0], Y[0]],
    [X[0], Y[1]], [X[1], Y[1]],
    [X[0], Y[2]],   [X[2], Y[2]],
    [X[0], Y[3]],     [X[3], Y[3]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)

# Write
if Save:
    for E in Ext: Fig.savefig(f'{SourcesPath}/{FigName}-NoIMG.{E}', dpi=1200)
    FigSize_pt = Plot.InchToPoint(np.array(FigSize))
    svgc.Figure(str(FigSize_pt[0])+'pt', str(FigSize_pt[1])+'pt',
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-IMG.svg')),
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-NoIMG.svg'))
    ).save(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% Fig Hist Chs Avg ===========================================================
FigName = f'CiralliEtAl_FigHistChsAvg'

# Load data
ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
ERPsX = ERPsTraces['X']

AnimalTraces = (ERPsTraces['Animals'] == 'Control_06') * (Treatments == 'NaCl')
ERPMean = ERPsTraces['ERPMeans'][:,:,np.where(AnimalTraces)[0][0]]

# Override: ch5 is broken
ERPMean = ERPMean[:,[0,1,2,3,5,6,7]]
YLim = [ERPMean[:,4].min(), ERPMean[:,4].max()]


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.5]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(2,1)
GSTop = GS[0].subgridspec(1,2)
GSBottom = GS[1].subgridspec(2,3,height_ratios=(0.3,0.7))
AxHist = Fig.add_subplot(GSTop[0])
AxesTraces = np.array([
    Fig.add_subplot(GSTop[1]),
    Fig.add_subplot(GSBottom[:,0]),
    Fig.add_subplot(GSBottom[1,1]),
    Fig.add_subplot(GSBottom[1,2]),
])
AxTL = Fig.add_subplot(GSBottom[0,1:])


# Histology
TA = {'va':'bottom','ha':'center'}
AxHist.axis('off')
AxHist.set_xlim((0,1))
AxHist.set_ylim((0,1))
AxHist.text(0.70, 0.85, 'dHipp', TA)

TA = {'va':'top','ha':'center'}
AA = dict(arrowstyle="->, head_width=0.3", lw=1.5, shrinkA=0)
ALen = 0.25
Axy = (0.9, 0.1)
AxHist.annotate('', xy=(Axy[0]-ALen, Axy[1]), xytext=Axy, arrowprops=AA)
AxHist.annotate('', xy=(Axy[0], Axy[1]+ALen), xytext=Axy, arrowprops=AA)
AxHist.text(Axy[0]-ALen/2, Axy[1]*0.9, 'Lateral', TA)

TA = {'va':'center','ha':'center'}
AxHist.text(Axy[0]*1.04, Axy[1]+ALen/2, 'Dorsal', TA, rotation=90)


# All ch
AxesTraces[0] = Plot.AllCh(
    ERPMean[:,:8], ERPsX, Labels=[1,2,3,4,6,7,8],
    ScaleBar=200, SpaceAmpF=1, lw=1,
    Ax=AxesTraces[0], AxArgs={'xticks': np.linspace(ERPsX[0],ERPsX[-1],4)}
)

SquareY = AxesTraces[0].get_lines()[5].get_ydata()
SquareY = [SquareY.min(), SquareY.max(), SquareY.max(), SquareY.min(), SquareY.min()]
SquareX = [ERPsX[0], ERPsX[0], ERPsX[-1], ERPsX[-1], ERPsX[0]]
AxesTraces[0].plot(SquareX, SquareY, color='silver', linestyle='--')

Scale = AxesTraces[0].get_lines()[0].get_xydata()
Scale[:,1] -= 1500
AxesTraces[0].get_lines()[0].set_xdata(Scale[:,0])
AxesTraces[0].get_lines()[0].set_ydata(Scale[:,1])
AxesTraces[0].text(Scale[0,0]-0.05, np.mean(Scale[:,1]), '200µV', rotation=90, ha='center', va='center', fontsize=8)

AxesTraces[0].set_xlabel('Time [s]')
AxesTraces[0].set_ylabel('Channels')
Plot.Set(Ax=AxesTraces[0])


# Chosen ch
AxesTraces[1].plot([ERPsX[ERPsX>=0.04][0]*1000]*2, YLim, 'k--', lw=0.7)
AxesTraces[1].plot([ERPsX[ERPsX>=0.08][0]*1000]*2, YLim, 'k--', lw=0.7)

AxesTraces[1].plot(
    ERPsX[(0.2>ERPsX)*(ERPsX>=-0.1)]*1000,
    ERPMean[(0.2>ERPsX)*(ERPsX>=-0.1),4],
    'k', lw=1, label='Click 1'
)
AxesTraces[1].plot(
    ERPsX[(0.2>ERPsX)*(ERPsX>=-0.1)]*1000,
    ERPMean[(0.7>ERPsX)*(ERPsX>=0.4),4],
    'r', lw=1, label='Click 2'
)

AxesTraces[1].set_title('Channel 6', pad=-8)
AxesTraces[1].set_xlabel('Time [ms]')
AxesTraces[1].set_ylabel('Voltage [µv]')
AxesTraces[1].legend(loc='lower left', bbox_to_anchor=(-0.00,0,1,1), **LA)
Plot.Set(Ax=AxesTraces[1])

AxesTraces[1].spines['bottom'].set_visible(False)
AxesTraces[1].set_xticks([])
for T in zip([40,80], [YLim[0]+YLim[0]*0.1]*2, ['40', '80']):
    AxesTraces[1].text(T[0], T[1], T[2], ha='center', va='top', fontsize=8)

AxesTraces[1].xaxis.labelpad = 20


# Timeline
TA = {'size':8, 'va':'bottom','ha':'center'}
AxTL.text(10, 0.05, 'Noise exposure\nor Sham exposure', TA)
AxTL.set_xlim((4,27))
AxTL.set_xticks((10,21))
AxTL.set_xticklabels(('Day 10', 'Day 21'))
AxTL.set_ylim((0,1))
AxTL.set_yticks(())
AxTL.spines['left'].set_visible(False)
Plot.Set(Ax=AxTL)


# Avg traces
KeyData = 'DataChannels'
KeyPeak = 'N40'
PeakLabels = ['1st click', '2nd click']

KeyPeaks = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/Peaks.dat')[0].tolist()
KeyInd = KeyPeaks.index(KeyPeak)

ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/{K}.dat')[0]
    for K in ['ControlRatios', 'TinnitusRatios', 'ControlLatencies', 'TinnitusLatencies']
]

AnimalCtr = ERPsTraces['Animals'] == 'Control_06'
AnimalTinn = ERPsTraces['Animals'] == 'Tinnitus_01'
ERPsX = ERPsTraces['X']
Groups = np.array([_.split('_')[0] for _ in ERPsTraces['Animals']])

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)


XLim = [round(ERPsX[0], 3), round(ERPsX[-1], 3)]
ColorsT = [Colors['Control'], Colors['Tinnitus']]
for C, Cond in enumerate([ControlAnimals, ~ControlAnimals]):
    Ind = C+2
    CondTreat = Cond
    Mean = ERPsTraces[KeyData][:,CondTreat].mean(axis=1)

    SLim = [
        ERPsTraces[KeyData][:,CondTreat].min(),
        ERPsTraces[KeyData][:,CondTreat].max()
    ]

    AxesTraces[Ind].plot([0,0], SLim, color=Colors['1stClick'], linestyle='--')
    AxesTraces[Ind].plot([0.5,0.5], SLim, color=Colors['2ndClick'], linestyle='--')
    AxesTraces[Ind].plot(ERPsX, ERPsTraces[KeyData][:,CondTreat], 'silver', lw=0.7)
    AxesTraces[Ind].plot(ERPsX, Mean, color=ColorsT[C], lw=1)

    PFun = np.argmin if KeyPeak == 'N40' else np.argmax
    PeaksX = [
        PFun(Mean[P])+(np.where((ERPsX>=0.5))[0][0]*p)
        for p,P in enumerate((ERPsX<0.1,ERPsX>=0.5))
    ]

    AxesTraces[Ind].plot(ERPsX[PeaksX], Mean[PeaksX], 'm--')
    AxesTraces[Ind].text(
        np.mean(ERPsX[PeaksX]), min(Mean[PeaksX]), 'N40',
        dict(color='m', va='top', ha='center')
    )

    AxArgs = {
        'xlim': XLim,
        'xticks': [-0.5, 0, 0.5, 1],
        'xlabel': 'Time [s]',
        'title': GroupsNames[C]
    }
    Plot.Set(Ax=AxesTraces[Ind], AxArgs=AxArgs)

    AxesTraces[Ind].yaxis.set_visible(False)
    AxesTraces[Ind].spines['left'].set_visible(False)


# Set fig
Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)

Pos = np.array(AxTL.get_position().bounds).T
Pos[1] *= 0.95
AxTL.set_position(Pos)



# Add sublabels
Pos = np.array([_.get_position().corners() for _ in AxesTraces]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0, Pos[0][0]-0.06, Pos[2][0]-0.03]
Y = [0.96, Pos[1][1]]
AxesPos = [
    [X[0], Y[0]], [X[1], Y[0]],
    [X[0], Y[1]], [X[2], Y[1]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write fig
if Save:
    Fig.savefig(f'{SourcesPath}/{FigName}-NoIMG.svg')
    FigSize_pt = Plot.InchToPoint(np.array(FigSize))
    svgc.Figure(str(FigSize_pt[0])+'pt', str(FigSize_pt[1])+'pt',
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-NoIMG.svg')),
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-IMG.svg'))
    ).save(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% Figure Avg Ratio N40 =======================================================
KeyPeak = 'N40'
FigName = f'CiralliEtAl_FigAvgRatio{KeyPeak}'

KeyData = 'DataChannels'
PeakLabels = ['1st click', '2nd click']
PlotType = ['scatter']
RatiosLabels = ['2nd peak suppr. [%]', '2nd peak delay [%]']
SMArgs = dict(Alpha=0.4, ErrorArgs=dict(capsize=3))


# Load data
KeyPeaks = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/Peaks.dat')[0].tolist()
KeyInd = KeyPeaks.index(KeyPeak)

ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
PeakAmps = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakAmps.dat')[0]
PeakAmps = abs(PeakAmps)
PeakLatencies = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakLatencies.dat')[0]
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
PVals =  IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/ERPs{KeyPeak}_1x2.dict')
PVals = IO.Txt.DictListsToArrays(PVals)

Animals = ERPsTraces['Animals']
AnimalCtr = ERPsTraces['Animals'] == 'Control_06'
AnimalTinn = ERPsTraces['Animals'] == 'Tinnitus_01'
ERPsX = ERPsTraces['X']
Groups = np.array([_.split('_')[0] for _ in Animals])

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)

ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/{K}.dat')[0]
    for K in ['ControlRatios', 'TinnitusRatios', 'ControlLatencies', 'TinnitusLatencies']
]


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.6]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(2,1,height_ratios=(1/3,2/3))
GSTraces = GS[0].subgridspec(2,5,width_ratios=(0.2,)+(0.8,)*4)
GSQnt = GS[1].subgridspec(2,2,width_ratios=(2/3,1/3))
AxesTraces = np.array([
    Fig.add_subplot(GSTraces[0,1]),
    Fig.add_subplot(GSTraces[0,2]),
    Fig.add_subplot(GSTraces[0,3]),
    Fig.add_subplot(GSTraces[0,4]),
    Fig.add_subplot(GSTraces[1,1]),
    Fig.add_subplot(GSTraces[1,2]),
    Fig.add_subplot(GSTraces[1,3]),
    Fig.add_subplot(GSTraces[1,4]),
])
AxesQnt = np.array([
    Fig.add_subplot(GSQnt[0,0]),
    Fig.add_subplot(GSQnt[1,0]),
    Fig.add_subplot(GSQnt[0,1]),
    Fig.add_subplot(GSQnt[1,1]),
])


# Traces
XLim = [round(ERPsX[0], 3), round(ERPsX[-1], 3)]
ColorsT = [Colors['Control'], Colors['Tinnitus']]
for C, Cond in enumerate([ControlAnimals, ~ControlAnimals]):
    for T, Treatment in enumerate(TreatmentsOrder):
        Ind = (C*len(TreatmentsOrder))+T
        CondTreat = Cond * (Treatments == Treatment)
        Mean = ERPsTraces[KeyData][:,CondTreat].mean(axis=1)

        SLim = [
            ERPsTraces[KeyData][:,CondTreat].min(),
            ERPsTraces[KeyData][:,CondTreat].max()
        ]
        AxesTraces[Ind].plot([0,0], SLim, color=Colors['1stClick'], linestyle='--')
        AxesTraces[Ind].plot([0.5,0.5], SLim, color=Colors['2ndClick'], linestyle='--')
        AxesTraces[Ind].plot(ERPsX, ERPsTraces[KeyData][:,CondTreat], 'silver', lw=0.7)
        AxesTraces[Ind].plot(ERPsX, Mean, color=ColorsT[C], lw=1)

        AxArgs = {'xlim': XLim, 'xticks': [-0.5, 0, 0.5, 1]}
        Plot.Set(Ax=AxesTraces[Ind], AxArgs=AxArgs)

        if Ind <4: AxesTraces[Ind].axis('off')
        else:
            AxesTraces[Ind].yaxis.set_visible(False)
            AxesTraces[Ind].spines['left'].set_visible(False)

        if not (C+T):
            TA = dict(size=7, va='bottom', ha='center')
            AxesTraces[Ind].text(0, SLim[1], '1', color=Colors['1stClick'], **TA)
            AxesTraces[Ind].text(0.5, SLim[1], '2', color=Colors['2ndClick'], **TA)

            PFun = np.argmin if KeyPeak == 'N40' else np.argmax
            PeaksX = [
                PFun(Mean[P])+(np.where((ERPsX>=0.5))[0][0]*p)
                for p,P in enumerate((ERPsX<0.1,ERPsX>=0.5))
            ]

            AxesTraces[Ind].plot(ERPsX[PeaksX], Mean[PeaksX], 'm--')
            AxesTraces[Ind].text(
                np.mean(ERPsX[PeaksX]), min(Mean[PeaksX]), 'N40',
                dict(color='m', va='top', ha='center', size=7)
            )

for A in range(4,8): AxesTraces[A].set_xlabel('Time [s]')
for A in range(4): AxesTraces[A].set_title(TreatmentsOrder[A])


# Ratios
ColorsC = [
    Colors['Control'].copy(),
    Colors['Tinnitus'].copy()
]
if KeyPeak == 'N40':
    ColorsC[0] /= 2
    ColorsC[1] *= 2
elif KeyPeak == 'P80':
    ColorsC[0] = [50/255, 0, 50/255]
    ColorsC[1] = [0, 0, 170/255]

ColorsCc = [Colors['1stClick'], Colors['2ndClick']]

XPos = [[0,3,6,9], [1,4,7,10]]
YPos = [[i*3,i*3+1] for i in range(len(TreatmentsOrder))]
XTicks = [np.mean(_) for _ in YPos]
SigSpace = [[1.15, 1.05], [1.05, 1.05]]

AnovaRatios = IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/PeakAmpLat{KeyPeak}.dict')
AnovaRatios = IO.Txt.DictListsToArrays(AnovaRatios)

AL = ('Amp. ratio', 'Lat. ratio')
for A, Ax in enumerate([[ControlRatios[:,:,KeyInd]*-1, TinnitusRatios[:,:,KeyInd]*-1],
                        [ControlLatencies[:,:,KeyInd], TinnitusLatencies[:,:,KeyInd]]]):

    Anova = AnovaRatios[A]

    for B in range(2):
        Plot.ScatterMean(
            Ax[B].T, XPos[B], ColorsMarkers=[ColorsT[B%2]]*4, Markers=['o']*4,
            Ax=AxesQnt[A], **SMArgs
        )

    AxesQnt[A] = AnovaStars(Anova, AxesQnt[A], None, 1.06)

    AxesQnt[A].set_ylabel(RatiosLabels[A])
    AxesQnt[A].set_xlim([XPos[0][0]-0.5, XPos[-1][-1]+0.5])
    AxesQnt[A].set_xticks(np.mean(XPos, axis=0))

    AxArgs = {
        'xlim': [-0.5, 10.5],
        'xticklabels': [_.replace('Cann+Nic','C+N') for _ in TreatmentsOrder],
        'title': AL[A%2]
    }
    if KeyPeak == 'N40':
        if A == 0: AxArgs['ylim'] = [-100,150]
        elif A == 1: AxArgs['ylim'] = [-100,200]

    if KeyPeak == 'P80':
        if A == 0: AxArgs['ylim'] = [-200,200]
        elif A == 1: AxArgs['ylim'] = [-100,100]

    Plot.Set(Ax=AxesQnt[A], AxArgs=AxArgs)


# Amplitudes
SigSpace = [1.05, 1.05]

AxesQnt[2] = Plot.ScatterMean(
    [
        PeakAmps[:,:,KeyInd][:,ControlAnimals[:]].ravel(),
        PeakAmps[:,:,KeyInd][:,~ControlAnimals[:]].ravel()
    ],
    ColorsMarkers=[Colors['Control'], Colors['Tinnitus']], Alpha=0.2, Markers=['o']*2, Ax=AxesQnt[2]
)

AR = IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/ERPs-Amp-Group-{KeyPeak}.dict')
AR = IO.Txt.DictListsToArrays(AR)
p = '{:.1e}'.format(AR[Stats.sak(AR)]['p'][0])
y = max(AxesQnt[2].get_ylim())
Plot.SignificanceBar([0,1], [y*SigSpace[0]]*2, p, AxesQnt[2], TextArgs=SigA, TicksDir=None)

AxesQnt[2].set_xticks((0,1))
AxesQnt[2].set_xticklabels([GroupsNames[0], GroupsNames[1].replace(GroupsBreak[1],GroupsBreak[1]+'\n')])
AxesQnt[2].set_ylabel('Voltage [µV]')
AxesQnt[2].set_title('Amplitude N40')
Plot.Set(Ax=AxesQnt[2])


# Latencies
AxesQnt[3] = Plot.ScatterMean(
    [
        PeakLatencies[:,:,KeyInd][:,ControlAnimals[:]].ravel(),
        PeakLatencies[:,:,KeyInd][:,~ControlAnimals[:]].ravel()
    ],
    ColorsMarkers=[Colors['Control'], Colors['Tinnitus']], Alpha=0.2, Markers=['o']*2, Ax=AxesQnt[3]
)

AR = IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/ERPs-Lat-Group-{KeyPeak}.dict')
AR = IO.Txt.DictListsToArrays(AR)
p = '{:.1e}'.format(AR[Stats.sak(AR)]['p'][0])
y = max(AxesQnt[3].get_ylim())
Plot.SignificanceBar([0,1], [y*SigSpace[0]]*2, p, AxesQnt[3], TextArgs=SigA, TicksDir=None)

AxesQnt[3].set_xticks((0,1))
AxesQnt[3].set_xticklabels([GroupsNames[0], GroupsNames[1].replace(GroupsBreak[1],GroupsBreak[1]+'\n')])
AxesQnt[3].set_ylabel('Latency [ms]')
AxesQnt[3].set_ylim((0,60))
AxesQnt[3].set_title('Latency N40')
Plot.Set(Ax=AxesQnt[3])


# Set axes
LegLoc = 'upper left'
LegBbox = (0, 0.0, 1.0, 0.95) if KeyPeak == 'N40' else (0, 0.1, 1.0, 0.9)
Legend = [
    Plot.GenLegendHandle(
        color=ColorsC[0], alpha=0.4, label=GroupsNames[0],
        marker='.', linestyle=''
    ),
    Plot.GenLegendHandle(
        color=ColorsC[1], alpha=0.4,
        label=GroupsNames[1].replace(GroupsBreak[1],GroupsBreak[1]+'\n'),
        marker='.', linestyle=''
    )
]
AxesQnt[1].legend(
    loc=LegLoc, handles=Legend, handlelength=1.0,
    bbox_to_anchor=LegBbox, labelcolor=ColorsT, **LA
)


Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)

Pos = np.array([_.get_position().bounds for _ in AxesQnt.ravel()]).T
Pos[3,:] *= 0.95
for A,Ax in enumerate(AxesQnt.ravel()): Ax.set_position(Pos[:,A])


# Add text
Pos = np.array([_.get_position().corners() for _ in AxesTraces]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
FA = {'size': 8, 'va':'center', 'ha': 'center', 'weight': 'bold', 'rotation': 90}
Fig.text(
    Pos[0][0]-0.03, Pos[0][1]-0.05, GroupsNames[0],
    fontdict=FA, color=ColorsT[0]
)
Fig.text(
    Pos[0][0]-0.03, Pos[4][1]-0.04, i
    GroupsNames[1].replace(GroupsBreak[1],GroupsBreak[1]+'\n'),
    fontdict=FA, color=ColorsT[1]
)


# Add labels
Pos = np.array([_.get_position().corners() for _ in AxesQnt.ravel()]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.004, Pos[2][0]-0.10]
Y = [0.96, Pos[0][1]+0.01, Pos[1][1]+0.01]
AxesQntPos = [
    [X[0], Y[0]],
    [X[0], Y[1]], [X[1], Y[1]],
    [X[0], Y[2]], [X[1], Y[2]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesQntPos)]
Plot.SubLabels(Fig, AxesQntPos, Letters, FontArgs=SPLA)


# Write fig
if Save: Fig.savefig(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% Figure Ratio P80 ===========================================================
KeyPeak = 'P80'
FigName = f'CiralliEtAl_FigRatio{KeyPeak}'

FigSize = [A4Size[0], A4Size[1]*0.3]
KeyData = 'DataChannels'
PeakLabels = ['1st click', '2nd click']
PlotType = ['scatter']
SMArgs = dict(Alpha=0.4, ErrorArgs=dict(capsize=3))

ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
KeyPeaks = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/Peaks.dat')[0].tolist()
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
PeakAmps = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakAmps.dat')[0]
PeakLatencies = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakLatencies.dat')[0]
PVals =  IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/ERPs{KeyPeak}_1x2.dict')
PVals = IO.Txt.DictListsToArrays(PVals)

KeyInd = KeyPeaks.index(KeyPeak)
ERPsX = ERPsTraces['X']
Animals = ERPsTraces['Animals']
AnimalCtr = Animals == 'Control_06'
Groups = np.array([_.split('_')[0] for _ in Animals])
RatiosLabels = ['2nd peak suppr. [%]', '2nd peak delay [%]']

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)

ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/{K}.dat')[0]
    for K in ['ControlRatios', 'TinnitusRatios', 'ControlLatencies', 'TinnitusLatencies']
]

PeakAmps = abs(PeakAmps)
ColorsC = [
    Colors['Control'].copy(),
    Colors['Tinnitus'].copy()
]
if KeyPeak == 'N40':
    ColorsC[0] /= 2
    ColorsC[1] *= 2
elif KeyPeak == 'P80':
    ColorsC[0] = [50/255, 0, 50/255]
    ColorsC[1] = [0, 0, 170/255]

ColorsCc = [Colors['1stClick'], Colors['2ndClick']]

XPos = [[0,3,6,9], [1,4,7,10]]
YPos = [[i*3,i*3+1] for i in range(len(TreatmentsOrder))]
XTicks = [np.mean(_) for _ in YPos]
SigSpace = [[1.15, 1.05], [1.05, 1.05]]


# Set Fig
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(1,3)

AxesTraces = np.array([
    Fig.add_subplot(GS[0]),
])
AxesQnt = np.array([
    Fig.add_subplot(GS[1]),
    Fig.add_subplot(GS[2]),
])


# Traces
XLim = [round(ERPsX[0], 3), round(ERPsX[-1], 3)]
ColorsT = [Colors['Control'], Colors['Tinnitus']]
TracesSpace = 1.3
TracesTA = {'size':7}
TracesLW = 0.5
for C, Cond in enumerate([ControlAnimals, ~ControlAnimals]):
    CondTreat = Cond * (Treatments == 'NaCl')
    Mean = ERPsTraces[KeyData][:,CondTreat].mean(axis=1)
    PFun = np.argmin if KeyPeaks == 'N40' else np.argmax
    PeaksX = [
        PFun(Mean[P])+(np.where((ERPsX>=0.5))[0][0]*p)
        for p,P in enumerate((ERPsX<0.1,ERPsX>=0.5))
    ]

    AxesTraces[0].plot(ERPsX, Mean-C*TracesSpace, color=ColorsC[C], lw=TracesLW)
    AxesTraces[0].plot(ERPsX[PeaksX], Mean[PeaksX]-C*TracesSpace, color=ColorsC[C], lw=TracesLW)

    AxesTraces[0].text(
        ERPsX[-1], np.min(Mean-C*TracesSpace), GroupsNames[C],
        {**TracesTA,**{'ha': 'right', 'va':'top'}}, color=ColorsC[C]
    )

    PFun = min if KeyPeaks == 'N40' else max
    TextSpace = 1.3 if PFun(Mean[PeaksX]-C*TracesSpace) > 0 else 1
    AxesTraces[0].text(
        np.mean(ERPsX[PeaksX]), PFun(Mean[PeaksX]-C*TracesSpace)*TextSpace,
        KeyPeak, {**TracesTA,**{'ha':'center'}}, color=ColorsC[C]
    )

    AxesTraces[0].text(
        ERPsX[PeaksX][C], 0.3, str(C+1),
        {**TracesTA,**{'ha': 'center'}}, color=ColorsCc[C]
    )

    SLim = AxesTraces[0].get_ylim()
    AxesTraces[0].plot(
        [ERPsX[PeaksX][0]]*2, np.percentile(Mean-C*TracesSpace, [0,100]),
        color=Colors['1stClick'], linestyle='--'
    )
    AxesTraces[0].plot(
        [ERPsX[PeaksX][1]]*2, np.percentile(Mean-C*TracesSpace, [0,100]),
        color=Colors['2ndClick'], linestyle='--'
    )

    if not C:
        AxesTraces[0].plot([-0.5,0], [Mean.mean()]*2, 'k--')
        AxesTraces[0].plot([-0.5,ERPsX[PeaksX][0]], [Mean[PeaksX][0]]*2, 'k--')
        AxesTraces[0].plot([-0.5,ERPsX[PeaksX][1]], [Mean[PeaksX][1]]*2, 'r--')
        AA = dict(arrowstyle="<->, head_width=0.2, head_length=0.2", lw=1.5, shrinkA=0, color='k')
        AxesTraces[0].annotate('', xy=(-0.4,Mean.mean()), xytext=(-0.4,Mean[PeaksX][0]), arrowprops=AA)
        AA = dict(arrowstyle="<->, head_width=0.2, head_length=0.2", lw=1.5, shrinkA=0, color='r')
        AxesTraces[0].annotate('', xy=(-0.2,Mean.mean()), xytext=(-0.2,Mean[PeaksX][1]), arrowprops=AA)

AxesTraces[0].text(-0.3, 0.4, 'Click', TracesTA)

AxArgs = {'xlim': XLim, 'xticks': [-0.5, 0, 0.5, 1], 'xlabel': 'Time [s]'}
Plot.Set(Ax=AxesTraces[0], AxArgs=AxArgs)

for Ax in AxesTraces:
    Ax.yaxis.set_visible(False)
    Ax.spines['left'].set_visible(False)


# Ratios
AnovaRatios = IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/PeakAmpLat{KeyPeak}.dict')
AnovaRatios = IO.Txt.DictListsToArrays(AnovaRatios)
for A, Ax in enumerate([[ControlRatios[:,:,KeyInd]*-1, TinnitusRatios[:,:,KeyInd]*-1],
                        [ControlLatencies[:,:,KeyInd], TinnitusLatencies[:,:,KeyInd]]]):

    Anova = AnovaRatios[A]

    for B in range(2):
        Plot.ScatterMean(
            Ax[B].T, XPos[B], ColorsMarkers=[ColorsC[B%2]]*4, Markers=['.']*4,
            Ax=AxesQnt[A], **SMArgs
        )

    AxesQnt[A] = AnovaStars(Anova, AxesQnt[A], None)

    AxesQnt[A].set_ylabel(RatiosLabels[A], labelpad=0)
    AxesQnt[A].set_xlim([XPos[0][0]-0.5, XPos[-1][-1]+0.5])
    AxesQnt[A].set_xticks(np.mean(XPos, axis=0))
    Plot.Set(Ax=AxesQnt[A])


for A,Ax in enumerate(AxesQnt):
    AxArgs = {'xlim': [-0.5, 10.5]}

    if KeyPeak == 'N40':
        if A == 0: AxArgs['ylim'] = [-100,150]
        elif A == 1: AxArgs['ylim'] = [-100,200]

    if KeyPeak == 'P80':
        if A == 0: AxArgs['ylim'] = [-200,200]
        elif A == 1: AxArgs['ylim'] = [-100,100]

    Plot.Set(Ax=Ax, AxArgs=AxArgs)

LegLoc = 'lower right' if KeyPeak == 'N40' else 'upper left'
LegBbox = (0, 0.0, 1.0, 0.95) if KeyPeak == 'N40' else (0, 0.1, 1.0, 0.9)
Legend = [
    Plot.GenLegendHandle(
        color=ColorsC[0], alpha=0.4, label=GroupsNames[0],
        marker='.', linestyle=''
    ),
    Plot.GenLegendHandle(
        color=ColorsC[1], alpha=0.4,
        label=GroupsNames[1].replace(GroupsBreak[1],GroupsBreak[1]+'\n'),
        marker='.', linestyle=''
    )
]
AxesQnt[1].legend(
    loc=LegLoc, handles=Legend, handlelength=1.0,
    bbox_to_anchor=LegBbox, labelcolor=ColorsC, **LA
)

for Ax in AxesQnt.ravel():
    Ax.set_xticklabels([
        _.replace('Cann+Nic','C+N') for _ in TreatmentsOrder
    ])

ColorsT = [Colors['Control'], Colors['Tinnitus']]
FA = {'fontsize': 10, 'weight': 'bold'}
for A in range(2):
    AxesQnt[A].set_title(GroupsNames[A], {**FA, **{'color':ColorsC[A]}})

AL = ('Amp. ratio', 'Lat. ratio')
for A in range(2): AxesQnt[A].set_title(AL[A], **FA)

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add labels
Pos = np.array([
    _.get_position().corners()
    for _ in np.hstack((AxesTraces,AxesQnt)).ravel()
]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.004, Pos[1][0]-0.095, Pos[2][0]-0.1]
Y = [Pos[0][1]]
AxesQntPos = [
    [X[0], Y[0]], [X[1], Y[0]], [X[2], Y[0]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesQntPos)]
Plot.SubLabels(Fig, AxesQntPos, Letters, FontArgs=SPLA)

Pos = np.array([_.get_position().bounds for _ in AxesQnt.ravel()]).T


# Write
if Save:
    for E in Ext: Fig.savefig(f'{FiguresPath}/{FigName}.{E}')
plt.show()



#%% Figure Amp Lat Click =======================================================
KeyPeak = 'N40'
# KeyPeak = 'P80'
FigName = f'CiralliEtAl_FigAmpLatClick{KeyPeak}'

KeyData = 'DataChannels'
PeakLabels = ['1st click', '2nd click']
PlotType = ['scatter']
RatiosLabels = ['2nd peak suppr. [%]', '2nd peak delay [%]']
SMArgs = dict(Alpha=0.4, ErrorArgs=dict(capsize=3))

KeyPeaks = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/Peaks.dat')[0].tolist()
ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
PeakAmps = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakAmps.dat')[0]
PeakLatencies = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakLatencies.dat')[0]
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
PVals =  IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/ERPs{KeyPeak}_1x2.dict')
PVals = IO.Txt.DictListsToArrays(PVals)

KeyInd = KeyPeaks.index(KeyPeak)
Animals = ERPsTraces['Animals']
AnimalCtr = ERPsTraces['Animals'] == 'Control_06'
AnimalTinn = ERPsTraces['Animals'] == 'Tinnitus_01'
ERPsX = ERPsTraces['X']
Groups = np.array([_.split('_')[0] for _ in Animals])

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)

ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/{K}.dat')[0]
    for K in ['ControlRatios', 'TinnitusRatios', 'ControlLatencies', 'TinnitusLatencies']
]


# Set fig
FigSize = [A4Size[0], A4Size[1]*0.4]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
AxesQnt = np.array([
    Fig.add_subplot(2,2,1),
    Fig.add_subplot(2,2,2),
    Fig.add_subplot(2,2,3),
    Fig.add_subplot(2,2,4)
])


# Amplitudes and latencies
PeakAmps = abs(PeakAmps)
ColorsC = [
    Colors['Control'].copy(),
    Colors['Tinnitus'].copy()
]
if KeyPeak == 'N40':
    ColorsC[0] /= 2
    ColorsC[1] *= 2
elif KeyPeak == 'P80':
    ColorsC[0] = [50/255, 0, 50/255]
    ColorsC[1] = [0, 0, 170/255]

ColorsCc = [Colors['1stClick'], Colors['2ndClick']]

XPos = [[0,3,6,9], [1,4,7,10]]
YPos = [[i*3,i*3+1] for i in range(len(TreatmentsOrder))]
XTicks = [np.mean(_) for _ in YPos]
SigSpace = [[1.15, 1.05], [1.05, 1.05]]

for V,Var in enumerate([PeakAmps[:,:,KeyInd], PeakLatencies[:,:,KeyInd]]):
    for G,Group in enumerate([ControlAnimals, ~ControlAnimals]):
        I = V*2+G
        if V:
            if KeyPeak == 'N40':
                AxesQnt[I].fill_between(
                    (YPos[0][0],YPos[-1][-1]),
                    (20,20),(50,50),
                    color='k', lw=0, alpha=0.1
                )
                AxesQnt[I].text(
                    np.mean(AxesQnt[I].get_xlim()),
                    55,
                    f'{KeyPeak} component',
                    {'color':'k', 'alpha':0.3, 'ha':'center', 'va':'center'}
                )

        for Tr,Treatment in enumerate(TreatmentsOrder):
            Y = []
            for P, Peak in enumerate(PeakLabels):
                if not len(Y):
                    Y = Var[P, ((Treatments == Treatment)*Group)]
                    Y = Y.reshape((Y.shape[0], 1))
                else:
                    Y = np.vstack((Y.T, Var[P, ((Treatments == Treatment)*Group)])).T

            if 'scatter' in [_.lower() for _ in PlotType]:
                AxesQnt[I] = Plot.ScatterMean(
                    Y.T, YPos[Tr],
                    ColorsMarkers=ColorsCc, Markers=['.']*2,
                    Paired=True, Ax=AxesQnt[I], **SMArgs
                )

            p = PVals['p'][(PVals['V']==V)*(PVals['G']==G)*(PVals['Tr']==Tr)][0]

            if p < 0.05:
                y = max(AxesQnt[I].get_ylim())
                p = '{:.1e}'.format(p)
                Plot.SignificanceBar(
                    YPos[Tr], [y*SigSpace[V][G]]*2, p,
                    AxesQnt[I], TextArgs=SigA, TicksDir=None,
                    LineTextSpacing=1.06
                )

            AxesQnt[I].set_xticks(XTicks)

for A,Ax in enumerate(AxesQnt):
    AxArgs = {'xlim': [-0.5, 10.5]}

    if A<2: AxArgs['ylabel'] =  f'{KeyPeak} Amplitude [µV]'
    else: AxArgs['ylabel'] =  f'{KeyPeak} Latency [ms]'

    if KeyPeak != 'P80':
        if A<2: AxArgs['ylim'] =  [0,800]
        else: AxArgs['ylim'] =  [0,60]

    if KeyPeak == 'P80':
        if A<2: AxArgs['ylim'] =  [0,300]
        else: AxArgs['ylim'] =  [20,140]

    Plot.Set(Ax=Ax, AxArgs=AxArgs)


LegLoc = 'lower right' if KeyPeak == 'N40' else 'upper left'
LegBbox = (0, 0.0, 1.0, 0.95) if KeyPeak == 'N40' else (0, 0.1, 1.0, 0.9)
Legend = [
    Plot.GenLegendHandle(
        color=Colors['1stClick'], alpha=0.5, label='Click 1',
        marker='.', linestyle=''
    ),
    Plot.GenLegendHandle(
        color=Colors['2ndClick'], alpha=0.5, label='Click 2',
        marker='.', linestyle=''
    )
]
AxesQnt[0].legend(
    handles=Legend, handlelength=1.0, loc='upper left',
    bbox_to_anchor=(0, 0.1, 1.0, 0.9), labelcolor=ColorsCc, **LA
)

for Ax in AxesQnt.ravel():
    Ax.set_xticklabels([
        _.replace('Cann+Nic','C+N') for _ in TreatmentsOrder
    ])

Pos = np.array([_.get_position().bounds for _ in AxesQnt.ravel()]).T

FA = {'fontsize': 10, 'weight': 'bold'}
for A in range(2):
    AxesQnt[A].set_title(
        GroupsNames[A], {**FA, **{'color':Colors[GroupsOrder[A]]}}
    )

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


# Add labels
Pos = np.array([_.get_position().corners() for _ in AxesQnt]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.004]
Y = [Pos[0][1], Pos[2][1]+0.01]
AxesQntPos = [
    [X[0], Y[0]],
    [X[0], Y[1]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesQntPos)]
Plot.SubLabels(Fig, AxesQntPos, Letters, FontArgs=SPLA)


# Write
if Save: Fig.savefig(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% Figure Amp Lat Treat P80 ===================================================
# KeyPeaks = ['N40']
KeyPeaks = ['P80']
FigName = f'CiralliEtAl_FigAmpLatTreat{KeyPeaks[0]}'

ClickLabels = ['Click 1', 'Click 2']
PlotType = ['scatter']
ColorsC = [Colors['Control'], Colors['Tinnitus']]
ColorsCc = [Colors['1stClick'], Colors['2ndClick']]
VarOrder = ['Amplitude', 'Latency']
FigSize = [A4Size[0], A4Size[1]*0.5]

SigSpace = np.array([
    [1.05, 1.02, 1.05, 1.05],
    [1.15, 1.05, 1.05, 1.05],
])
SigSpace += 0.02

PeakLabels = ['1stClick', '2ndClick']
RatiosLabels = ['2nd peak suppr. [%]', '2nd peak delay [%]']

ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
PeakAmps = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakAmps.dat')[0]
PeakLatencies = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakLatencies.dat')[0]
KeyList = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/Peaks.dat')[0].tolist()

Animals = ERPsTraces['Animals']
AnimalCtr = Animals == 'Control_06'
Groups = np.array([_.split('_')[0] for _ in Animals])

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)

ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
    IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsRatiosLatencies/{K}.dat')[0]
    for K in ['ControlRatios', 'TinnitusRatios', 'ControlLatencies', 'TinnitusLatencies']
]

PeakAmps = abs(PeakAmps)

Fig, Axes = plt.subplots(len(KeyPeaks)*2, 4, figsize=FigSize, constrained_layout=True)

for P, PeakLabel in enumerate(PeakLabels):
    PVals =  IO.Txt.Read(f'{AnalysisPath}/GroupAnalysis/Stats/ERPsC{PeakLabel[0]}.dict')
    PVals = IO.Txt.DictListsToArrays(PVals)

    for KP, KeyPeak in enumerate(KeyPeaks):
        AxL = KP*2+P
        KeyInd = KeyList.index(KeyPeak)

        for V,Var in enumerate([PeakAmps, PeakLatencies]):
            for G,Group in enumerate([ControlAnimals, ~ControlAnimals]):
                AxC = (V*2)+G

                Y = []
                for Tr,Treatment in enumerate(TreatmentsOrder):
                    y = Var[:,:,KeyInd][:, ((Treatments == Treatment)*Group)]
                    if not len(Y):
                        Y = np.zeros(y.shape+(len(TreatmentsOrder),), y.dtype)

                    Y[:,:,Tr] = y

                if 'scatter' in [_.lower() for _ in PlotType]:
                    Axes[AxL][AxC] = Plot.ScatterMean(
                        Y[P,:,:].T, range(len(TreatmentsOrder)),
                        ColorsMarkers=[ColorsCc[P]]*len(TreatmentsOrder),
                        Markers=['.']*len(TreatmentsOrder), Paired=True,
                        Ax=Axes[AxL][AxC]
                    )

                Pairs = list(Stats.combinations(range(len(TreatmentsOrder)), 2))
                pKP = 0 if 'N40' in KeyPeaks else 1
                for Pa,Pair in enumerate(Pairs):
                    p = PVals['p'][(PVals['KP']==pKP)*(PVals['V']==V)*(PVals['G']==G)*(PVals['Pa']==Pa)][0]
                    if p < 0.05:
                        y = max(Axes[AxL][AxC].get_ylim())
                        ps = '{:.1e}'.format(p)
                        Plot.SignificanceBar(
                            Pair, [y*SigSpace[AxL][AxC]]*2, ps,
                            Axes[AxL][AxC], TextArgs=SigA,
                            TicksDir=None, LineTextSpacing=1.05
                        )

for L,Line in enumerate(Axes):
    for A,Ax in enumerate(Line):
        AxArgs = {
            'xlim': [-0.5,3.5],
            'xticks': range(4)
        }

        if KeyPeaks[0] == 'N40':
            if L == 0 and A < 2: AxArgs['ylim'] =  [0,800]
            elif L == 0 and A > 1:  AxArgs['ylim'] =  [0,60]
            elif L == 1 and A > 1:  AxArgs['ylim'] =  [0,60]
            elif L == 1 and A < 2:
                AxArgs['ylim'] =  [0,600]
                AxArgs['yticks'] = np.linspace(0,600,4)

        elif KeyPeaks[0] == 'P80':
            if L == 0 and A < 2:
                AxArgs['ylim'] =  [0,250]
                AxArgs['yticks'] = np.linspace(0,250,3)
            elif L == 0 and A > 1:
                AxArgs['ylim'] =  [20,140]
                AxArgs['yticks'] = np.linspace(20,140,4)
            elif L == 1 and A < 2:
                AxArgs['ylim'] =  [0,250]
                AxArgs['yticks'] = np.linspace(0,250,3)
            elif L == 1 and A > 1:  AxArgs['ylim'] =  [0,150]

        if A == 0: AxArgs['ylabel'] =  'Voltage [µV]'
        elif A == 2: AxArgs['ylabel'] =  'Latency [ms]'

        Plot.Set(Ax=Ax, AxArgs=AxArgs)

for A,Ax in enumerate(Axes.ravel()):
    Ax.set_xticklabels([])
    lp = 10 if KeyPeaks[0]=='N40' else 5
    Ax.set_title(
        GroupsNames[A%2].replace(GroupsBreak[1],GroupsBreak[1]+'\n'), pad=lp
    )

for Ax in Axes[1,:].ravel():
    Ax.set_xticklabels(
        [_.replace('Cann+Nic','C+N') for _ in TreatmentsOrder],
        rotation=-45
    )

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)
Fig.subplots_adjust(hspace=0.3, wspace=0.4, top=0.89, right=0.99, bottom=0.07)

Pos = np.array([_.get_position().bounds for _ in Axes.ravel()]).T
Pos[0,[1,5]] -= 0.02
Pos[0,[2,6]] += 0.02
Pos[3,:] *= 0.90
for A,Ax in enumerate(Axes.ravel()): Ax.set_position(Pos[:,A])


# Add boxes around subplot titles
Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [
    [
        Pos[0,1,_], Pos[1,1,_]*1.0175, Pos[0,2,_]-Pos[0,1,_],
        (Pos[1,1,_]-Pos[1,0,_])*0.125
    ]
    for _ in range(Pos.shape[2])
]

pax = (0,2)
for _ in pax:
    Pos[_][1] *= 1.1
    Pos[_][2] = (Pos[_+1][0]+Pos[_+1][2])-Pos[_][0]

AxRects = [Fig.add_axes(Pos[_], zorder=-1) for _ in pax]
for A,Ax in enumerate(AxRects):
    Ax.add_patch(plt.Rectangle((0,0),1,1, facecolor='w', edgecolor='k', clip_on=False, linewidth=1))
    Ax.text(0.5, 0.4, f'{KeyPeaks[A//2]} {VarOrder[A%2]}', ha='center', va='center')
    Ax.axis('off')

Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [[0.005, Pos[1,0,_], 0.025, Pos[1,1,_]-Pos[1,0,_]] for _ in range(Pos.shape[2])]

pax = (0,4)
for _ in pax: Pos[_][1]*=1
AxRects = [Fig.add_axes(Pos[_], zorder=-1) for _ in pax]
for A,Ax in enumerate(AxRects):
    Ax.add_patch(plt.Rectangle((0,0),1,1, facecolor='w', edgecolor=ColorsCc[A%2], clip_on=False, linewidth=1))
    Ax.text(0.6, 0.5, ClickLabels[A%2], color=ColorsCc[A%2], ha='center', va='center', rotation=90)
    Ax.axis('off')


# Add sublabels
Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.005, Pos[2][0]-0.08]
Y = [0.96]
AxesPos = [
    [X[0], Y[0]], [X[1], Y[0]],
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write
if Save:
    for E in Ext: Fig.savefig(f'{FiguresPath}/{FigName}.{E}')
plt.show()



#%% Figure Peak width and double peaks =========================================
FigName = f'CiralliEtAl_FigPeakWidth'
XPos = [[0,3,6,9], [1,4,7,10]]

YPos = [[i*3,i*3+1] for i in range(len(TreatmentsOrder))]
XTicks = [np.mean(_) for _ in YPos]
ColorsCc = [Colors['1stClick'], Colors['2ndClick']]
ColorsC = [
    Colors['Control'].copy(),
    Colors['Tinnitus'].copy()
]
if KeyPeak == 'N40':
    ColorsC[0] /= 2
    ColorsC[1] *= 2
elif KeyPeak == 'P80':
    ColorsC[0] = [50/255, 0, 50/255]
    ColorsC[1] = [0, 0, 170/255]


ERPsTraces = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsTraces')[0]
ERPsPeaks = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeaks')[0]
PeakWidth = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsPeakWidth.dat')[0]
Treatments = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Treatments.dat')[0]
Channels = IO.Bin.Read(f'{AnalysisPath}/GroupData/ERPsInfo/Channels.dat')[0]
AnimalCtr = ERPsTraces['Animals'] == 'Control_06'
AnimalTinn = ERPsTraces['Animals'] == 'Tinnitus_01'
Rate = ERPsTraces['Rates'][AnimalCtr][0]
X = ERPsTraces['X']

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)

PeakLabels = ['1st click', '2nd click']
Jitter = 0.0
ScaleBar = 0.1
AnimalTraces = [AnimalCtr,AnimalTinn]
SpaceAmpF = 0.2


# Set figure
FigSize = [A4Size[0], A4Size[1]*0.5]
Fig = plt.figure(figsize=FigSize, constrained_layout=True)
GS = Fig.add_gridspec(3,2)
Axes = np.array([
    Fig.add_subplot(GS[0,0]),
    Fig.add_subplot(GS[0,1]),
    Fig.add_subplot(GS[2,0]),
    Fig.add_subplot(GS[2,1]),
    Fig.add_subplot(GS[1,:]),
])


# Plot traces
YLim = [-400, 400]
ColorsCT = (ColorsCc,ColorsC)
for G,Group in enumerate([ControlAnimals, ~ControlAnimals]):
    XLong = []
    for Tr,Treatment in enumerate(TreatmentsOrder):
        Ind = AnimalTraces[G]*(Treatments==Treatment)
        RawChs = np.array(Channels)[Ind][0]-1
        Y = ERPsTraces['ERPMeans'][:,RawChs,Ind]
        Y = [Y[np.where((0<X)*(X<0.3))[0],0], Y[np.where((0.5<X)*(X<0.8))[0],0]]

        if not len(XLong):
            Size = Y[0].shape[0]
            XLong = np.arange(Size*4+Size*len(TreatmentsOrder)*SpaceAmpF)
            Starts = [int((Size+Size*SpaceAmpF)*_) for _ in range(len(TreatmentsOrder))]

        Axes[G].plot(XLong[Starts[Tr]:Starts[Tr]+Size], Y[0], color=ColorsCT[G][0], lw=0.8)
        Axes[G].plot(XLong[Starts[Tr]:Starts[Tr]+Size], Y[1], color=ColorsCT[G][1], lw=0.8)

        if True in ERPsPeaks['Doublets'][Ind,:]:
            y = np.where((ERPsPeaks['Doublets'][Ind,:][0] == True))[0][0]
            N40s = Analysis.GetPeaks(Y[y])['Neg'][0]
            N40sV = Y[y][N40s]*0.99

            dArrow = [N40s, N40sV]

            dArrow[0] = np.where((np.arange(Y[y].shape[0])>=dArrow[0]))[0][0]
            dArrow[0] = XLong[Starts[Tr]:Starts[Tr]+Size][dArrow[0]]*0.99

            OfSt = 0 if not G else 32000
            Axes[G].annotate(
                '', xy=[dArrow[0]-OfSt, dArrow[1]],
                xytext=[dArrow[0]-OfSt*1.032-500,dArrow[1]-100],
                arrowprops={
                    'shrinkA':0, 'shrinkB':0, 'arrowstyle':'->',
                    'edgecolor':Colors['Arrows'][()],'facecolor':Colors['Arrows'][()]
                }
            )


    End = 29600
    Y = -220
    Axes[G].plot([End-ScaleBar*Rate, End], [Y]*2, 'k', lw=2)
    Axes[G].text(End-(ScaleBar/2)*Rate, -250, str(ScaleBar)+'s', ha='center', va='top')

    if not G:
        Axes[G].plot([XLong[Starts[0]]+0.033*Rate]*2, [Y, 90], 'k|--', lw=0.8, zorder=1)
        Axes[G].plot([XLong[Starts[0]]+0.08*Rate]*2, [Y, 90], 'k|--', lw=0.8, zorder=1)

        for T in zip([XLong[Starts[0]]+0.033*Rate, XLong[Starts[0]]+0.08*Rate],
                     [-250, 150],['40','80']):
            Axes[G].text(T[0], T[1], T[2], va='top', ha='center', fontsize=8)


    NamesX = [Size//2+Starts[_] for _ in range(len(TreatmentsOrder))]
    for L,Label in enumerate(['NaCl', 'Nic', 'Cann', 'Cann\n+Nic']):
        Axes[G].text(NamesX[L], 350, Label, ha='center', va='center')

    AxArgs = {'ylim': YLim, 'yticks': np.linspace(min(YLim), max(YLim), 5, dtype=int)}
    Plot.Set(Ax=Axes[G], AxArgs=AxArgs)

for A in range(2):
    Axes[A].spines['bottom'].set_visible(False)
    Axes[A].set_xticks([])


# Plot Ratio
YLim = [PeakWidth.min()-PeakWidth.min()%10-10, PeakWidth.max()-(PeakWidth.max()%10)+10]
BLabel = PeakLabels[:]
for G,Group in enumerate([ControlAnimals, ~ControlAnimals]):
    XMeans, YMeans= [[],[]], [[],[]]

    for Tr,Treatment in enumerate(TreatmentsOrder):
        Y = []
        RawChs = np.array(Channels)[(AnimalTraces[G]*(Treatments==Treatment))][0]

        for P, Peak in enumerate(PeakLabels):
            if not len(Y):
                Y = PeakWidth[((Treatments == Treatment)*Group), P]
                Y = Y.reshape((Y.shape[0], 1))
            else:
                Y = np.vstack((Y.T, PeakWidth[((Treatments == Treatment)*Group), P])).T

        Y[Y==0] = Y[Y!=0].mean()

        Axes[G+2] = Plot.ScatterMean(Y.T, YPos[Tr], ColorsMarkers=ColorsCT[G], Markers=['.']*2, Paired=True, Ax=Axes[G+2])

    Axes[G+2].set_xlim([-0.5,10.5])
    Axes[G+2].set_xticks(XTicks)
    Axes[G+2].set_xticklabels(['NaCl', 'Nic', 'Cann', 'Cann\n+Nic'])
    Axes[G+2].set_ylim([0,105])
    Plot.Set(Ax=Axes[G+2])


# Plot doublets bars
XOr = [
    (P,Tr,G)
    for Tr,Treatment in enumerate(TreatmentsOrder)
    for G,Group in enumerate([ControlAnimals, ~ControlAnimals])
    for P, Peak in enumerate(PeakLabels)
]
XPos = [_+(a//2) for a in range(len(XOr)) for _ in (a*3,a*3+1)]

for Tr,Treatment in enumerate(TreatmentsOrder):
    for G,Group in enumerate([ControlAnimals, ~ControlAnimals]):
        for P, Peak in enumerate(PeakLabels):
            Color = ColorsCc[P] if not G else ColorsC[P]
            iB = XOr.index((P,Tr,G))
            Leg = Peak if Tr == G == 7 else ''
            YBar = np.where((ERPsPeaks['Doublets'][Group*(Treatments==Treatment),P]))[0].shape[0]
            YBar = YBar/(ERPsPeaks['Doublets'][Group*(Treatments==Treatment),P].shape[0])*100
            Axes[-1].bar(XPos[iB], YBar, color=Color, alpha=0.7, linewidth=2, edgecolor=Color, label=Leg)

Axes[-1].set_xticks(Analysis.MovingAverage(XPos)[1::4][:4])
Axes[-1].set_ylim((0,40))
Plot.Set(Ax=Axes[-1])


# Set axis
Axes[0].set_ylabel('Voltage [µV]')
for A in range(2): Axes[A].set_title(GroupsNames[A], pad=10)
Axes[2].set_ylabel('Width [ms]')

Axes[-1].set_xticklabels(['NaCl', 'Nic', 'Cann', 'Cann\n+Nic'])
Axes[-1].set_ylabel('Double peaks [%]')

Fig.get_layout_engine().execute(Fig)
Fig.set_layout_engine(None)


LLabels = ('Sham Click 1', 'Sham Click 2', f'{GroupsNames[1]} Click1', f'{GroupsNames[1]} Click2')
LineY = np.array((0.53,0.505,0.48,0.455))+0.14

LXs = np.array([0.73, 0.74])+0.02

Lines = [
    plt.Line2D([LXs[0], LXs[1]], [LineY[0]]*2, color='w', transform=Fig.transFigure, figure=Fig),
    plt.Line2D([LXs[0], LXs[1]], [LineY[1]]*2, color='w', transform=Fig.transFigure, figure=Fig),
    plt.Line2D([LXs[0], LXs[1]], [LineY[2]]*2, color='w', transform=Fig.transFigure, figure=Fig),
    plt.Line2D([LXs[0], LXs[1]], [LineY[3]]*2, color='w', transform=Fig.transFigure, figure=Fig),
]
Axes[-1].legend(Lines, LLabels, loc='upper right', labelcolor='w', bbox_to_anchor=(-0.015,0.21,1,1)).set_zorder(-1)

LXs = np.array([0.73, 0.74, 0.75, 0.765])-0.06

Lines = [
    plt.Line2D(
        [LXs[0], LXs[1]], [LineY[0],LineY[0]], color=ColorsCc[0],
        transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[2]], [LineY[0]], marker='.', color=ColorsCc[0],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[3]], [LineY[0]], marker='s', color=ColorsCc[0],
        alpha=0.5,  transform=Fig.transFigure, figure=Fig
    ),

    plt.Line2D(
        [LXs[0], LXs[1]], [LineY[1],LineY[1]], color=ColorsCc[1],
        transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[2]], [LineY[1]], marker='.', color=ColorsCc[1],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[3]], [LineY[1]], marker='s', color=ColorsCc[1],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),

    plt.Line2D(
        [LXs[0], LXs[1]], [LineY[2],LineY[2]], color=ColorsC[0],
        transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[2]], [LineY[2]], marker='.', color=ColorsC[0],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[3]], [LineY[2]], marker='s', color=ColorsC[0],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),

    plt.Line2D(
        [LXs[0], LXs[1]], [LineY[3],LineY[3]], color=ColorsC[1],
        transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[2]], [LineY[3]], marker='.', color=ColorsC[1],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),
    plt.Line2D(
        [LXs[3]], [LineY[3]], marker='s', color=ColorsC[1],
        alpha=0.5, transform=Fig.transFigure, figure=Fig
    ),
]
Fig.lines.extend(Lines)

TXs = LXs[0]+0.05
Fig.text(TXs, LineY[0], LLabels[0], fontsize=8, va='center')
Fig.text(TXs, LineY[1], LLabels[1], fontsize=8, va='center')
Fig.text(TXs, LineY[2], LLabels[2], fontsize=8, va='center')
Fig.text(TXs, LineY[3], LLabels[3], fontsize=8, va='center')


# add sublabels
Pos = np.array([_.get_position().corners() for _ in Axes.ravel()]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0.01]
Y = [0.96, Pos[-1][1], Pos[2][1]]
AxesPos = [
    [X[0], Y[0]],
    [X[0], Y[1]],
    [X[0], Y[2]]
]
Letters = Analysis.StrRange('A','Z')[:len(AxesPos)]
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write
if Save:
    for E in Ext: Fig.savefig(f'{FiguresPath}/{FigName}.{E}')
plt.show()



#%% Fig PCAs ===================================================================
FigName = f'CiralliEtAl_FigPCAs'
PCs = ['pc1', 'pc4']
PCsScore = [22.3, 9.5]
MarkersT = ['o', '^', 's', '+']

PCAs = {
    F.split('/')[-1].split('.')[0]: np.loadtxt(F, dtype=str, delimiter=',')
    for F in glob(f'{AnalysisPath}/GroupData/PCAs/*.csv')
}


FigSize = [A4Size[0], A4Size[1]*0.7]
Fig = plt.figure(figsize=FigSize)
GridSpec, GridSpecFromSubplotSpec = (
    Plot.Return(_) for _ in ('GridSpec', 'GridSpecFromSubplotSpec')
)
GSPCs = GridSpec(len(PCs)+1,1)
GSTop = GridSpecFromSubplotSpec(1,2, subplot_spec=GSPCs[0])
GSBars = [GridSpecFromSubplotSpec(1,4, subplot_spec=GSPCs[_+1]) for _ in range(len(PCs))]
Axes = np.array(
    [plt.Subplot(Fig, GSTop[0]), plt.subplot2grid((len(PCs)+1,2), (0,1), projection='3d')] +
    [
        _
        for e in range(len(PCs))
        for _ in [
            plt.Subplot(Fig, GSBars[e][0,:2]),
            plt.Subplot(Fig, GSBars[e][0,2]),
            plt.Subplot(Fig, GSBars[e][0,3]),
        ]
    ]
)
for Ax in Axes: Fig.add_subplot(Ax)

for G,Group in enumerate(GroupsOrder):
    for T,Treatment in enumerate(TreatmentsOrder):
        Ind = np.where(
            (PCAs[f'scores_fpca_lfp'][:,2]==f'"{Group}"') *
            (PCAs[f'scores_fpca_lfp'][:,3]==f'"{Treatment}"')
        )[0]

        Axes[0].scatter(
            PCAs[f'scores_fpca_lfp'][Ind,4].astype(float),
            PCAs[f'scores_fpca_lfp'][Ind,5].astype(float),
            color = Colors[Group],
            marker = MarkersT[T]
        )

        Axes[1] = Plot.Scatter3D(
            PCAs[f'scores_fpca_lfp'][Ind,4:7].astype(float),
            {'c': Colors[Group], 'marker': MarkersT[T]}, ScaleBarArgs={}, Ax=Axes[1]
        )

for P,PC in enumerate(PCs):
    for G,Group in enumerate(GroupsOrder):
        for T,Treatment in enumerate(TreatmentsOrder):
            Ind = np.where(
                (PCAs[f'lfp_{PC}_effects'][:,1]==f'"{Group}"') *
                (PCAs[f'lfp_{PC}_effects'][:,2]==f'"{Treatment}"')
            )[0]

            Axes[3+(P*3)+G].plot([-0.5, len(TreatmentsOrder)-0.5], [0,0], 'k--', lw=0.8)
            Axes[3+(P*3)+G].bar(T, PCAs[f'lfp_{PC}_effects'][Ind,4].astype(float), yerr=PCAs[f'lfp_{PC}_effects'][Ind,5].astype(float), color=Colors[Group], capsize=3)

for A,Ax in enumerate(Axes):
    AxArgs = {}

    if A == 0:
        AxArgs['xlim'] = [-2, 3]
        AxArgs['ylim'] = [-1.5, 2.5]
        AxArgs['xlabel'] = 'PC1 (22.3%)'
        AxArgs['ylabel'] = 'PC2 (19%)'

    if A == 1:
        AxArgs['xlabel'] = 'PC1 (22.3%)'
        AxArgs['ylabel'] = 'PC2 (19%)'
        AxArgs['zlabel'] = 'PC3 (12.3%)'

    if A in [_*3+2 for _ in range(len(PCs))]:
        IndPC = (A-2)//3
        AxArgs['xlim'] = [-200, 1000]
        AxArgs['ylim'] = [-0.4, 0.2]
        AxArgs['yticks'] = np.linspace(-0.4, 0.2, 4).round(2)
        AxArgs['xlabel'] = 'Time [s]'
        AxArgs['ylabel'] = f'Harmonic {PCs[IndPC][-1]}'
        AxArgs['title'] = f'{PCs[IndPC].upper()} ({PCsScore[IndPC]}%)'

    if A in [_ for e in range(len(PCs)) for _ in (e*len(PCs)+3,e*len(PCs)+4)][:-2]:
        AxArgs['xticks'] = range(len(TreatmentsOrder))

    if A in list(range(len(Axes)))[-2:]:
        AxArgs['xticks'] = range(len(TreatmentsOrder))

    Plot.Set(Ax=Ax, AxArgs=AxArgs)

    if A in [_*3+3 for _ in range(len(PCs))]:
        IndPC = (A-3)//3
        Ax.set_ylabel(f'{PCs[IndPC].upper()}', labelpad=-5)

    if A in (3,4,6,7):
        Ax.set_xticklabels(TreatmentsOrder, rotation=45)

    if A == 1:
        Ax.set_zlabel('PC3 (12.3%)')


Legends = [
    Plot.GenLegendHandle(color=Colors[Group], label=GroupsNames[G].replace(GroupsBreak[1],GroupsBreak[1]+'\n'))
    for G,Group in enumerate(GroupsOrder)
]
Legends += [
    Plot.GenLegendHandle(color='k', linestyle='', marker=MarkersT[T], label=Treatment)
    for T,Treatment in enumerate(TreatmentsOrder)
]

Axes[0].legend(
    handles=Legends,
    labelcolor=[Colors['Control'], Colors['Tinnitus']]+['k']*4,
    bbox_to_anchor=(0.46, -0.1, 1.0, 1.0),
    **{'borderpad': 0.1, 'borderaxespad': 0.1}
)

Plot.Set(Fig=Fig)
Fig.subplots_adjust(wspace=0.6)


Pos = np.array([_.get_position().corners() for _ in Axes]).T
Pos = [Pos[:,1,_] for _ in range(Pos.shape[2])]
X = [0, Pos[3][0]-0.08]
Y = [0.97, Pos[2][1]+0.03, Pos[5][1]+0.03]
AxesPos = [
    [X[0], Y[0]], [X[1],Y[0]],
    [X[0], Y[1]], [X[1],Y[1]],
    [X[0], Y[2]], [X[1],Y[2]],
]
Letters = ['A','B','C','D','E','F']
Plot.SubLabels(Fig, AxesPos, Letters, FontArgs=SPLA)


# Write
if Save:
    FigSize_pt = Plot.InchToPoint(np.array(FigSize))
    Fig.savefig(f'{SourcesPath}/{FigName}-NoIMG.svg')
    svgc.Figure(str(FigSize_pt[0])+'pt', str(FigSize_pt[1])+'pt',
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-NoIMG.svg')),
        svgc.Panel(svgc.SVG(f'{SourcesPath}/{FigName}-IMG.svg'))
    ).save(f'{FiguresPath}/{FigName}.svg')
plt.show()



#%% EOF ========================================================================
