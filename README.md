# SensoryGatingOnTinnitus2022

Ciralli, Barbara, Malfatti, Thawann, & Lima, Thiago Zaqueu. (2022). SensoryGatingOnTinnitus2022 (v1.0.0). Zenodo. https://doi.org/10.5281/zenodo.6645915

This repository contains all code used for the experiments and analysis of the publication

Alterations of auditory sensory gating in mice with noise-induced tinnitus treated with nicotine and cannabis extract
(under review)

