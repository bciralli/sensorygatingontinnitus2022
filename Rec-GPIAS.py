#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@year: 2019
@license: GNU GPLv3 <https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022/raw/main/LICENSE>
@homepage: https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022
"""



#%% Prepare and run GPIAS ======================================================
from sciscripts.Exps import GPIAS

Parameters = dict(
    ## === Animal  === ##
    AnimalName  = 'Tinnitus_11',
    CageName    = 'B2',

    ## === Sound  === ##
    StimType                = ['Sound'],
    NoOfTrials              = 9,
    SoundBGDur              = 2.3,
    SoundGapDur             = 0.04,
    SoundBGPrePulseDur      = 0.1,
    SoundLoudPulseDur       = 0.05,
    SoundBGAfterPulseDur    = 0.51,
    SoundBetweenStimDur     = [12, 22],

    NoiseFrequency          = [
        [8000, 10000],
        [9000, 11000],
        [10000, 12000],
        [12000, 14000],
        [14000, 16000],
        [8000, 18000]
    ],

    BGIntensity     = [60],
    PulseIntensity  = [105],


    ## === Hardware  === ##
    StimCh          = 2,
    TTLCh           = 1,
    RecCh           = [3,4,5],
    AnalogTTLs      = True,

    System     = 'Jack-IntelOut-Marantz-IntelIn',
    Setup           = 'GPIAS',
)

GPIAS.Run(**Parameters)



#%% EOF ========================================================================
