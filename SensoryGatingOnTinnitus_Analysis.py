#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: T. Malfatti and B. Ciralli
@date: 20190502
@license: GNU GPLv3 <https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022/raw/main/LICENSE>
@homepage: https://gitlab.com/BCiralli/SensoryGatingOnTinnitus2022
"""



#%% Prepare ====================================================================
ScriptName = 'SensoryGatingOnTinnitus'

print(f'[{ScriptName}] Loading dependencies...')
import gc, numpy as np, os
from glob import glob
from multiprocessing import Array as mpArray, Lock as mpLock
from sciscripts.Analysis import ABRs, ERPs, GPIAS, Stats
from sciscripts.IO import DAqs, IO

from sciscripts.Analysis.Plot import Plot
from sciscripts.Analysis import Analysis
plt = Plot.Return('plt')

DataPath = '/run/media/malfatti/Helvault/Ciralli/Documents/Data/SensoryGatingOnTinnitus'
AnalysisPath = '/run/media/malfatti/Helvault/Ciralli/Documents/Analysis/SensoryGatingOnTinnitus'
AnalysisSharedPath = os.environ['CALIGOPATH']+'/ArlinnKord/Documents/Analysis/SensoryGatingOnTinnitus'

Groups = ('Control','Tinnitus')
Epochs = ('BeforeANT', 'AfterANT')


print(f'[{ScriptName}] Done.')



#%% [ABRs] Get traces ==========================================================
Folders = sorted(glob(DataPath+'/Control/*-ABRs') + glob(DataPath+'/Tinnitus/*-ABRs'))
Log = {_: [] for _ in ['Done', 'Error', 'Exception']}

for F,Folder in enumerate(Folders):
    SubFolders = sorted(glob(Folder+'/*20??-*'))
    SubFolders = [_ for _ in SubFolders if 'impedance' not in _.lower() and _[-4:] != 'dict']


    InfoFile = glob(Folder+'/*.dict')[-1]
    Info = IO.Txt.Read(InfoFile)
    if 'Audio' not in Info.keys():
        Info = IO.Txt.Dict_OldToNew(Info)
        IO.Txt.Write(Info, InfoFile)

    Freqs = [Info['ExpInfo'][K]['Hz'] for K in sorted(list(Info['ExpInfo'].keys()))]
    Intensities = [str(_) for _ in Info['Audio']['Intensities']]
    Exps = [
        int(k) for k,K in Info['ExpInfo'].items()
            if K['Hz'].lower() not in ['bl', 'baseline']
            and 'Sound' in K['StimType']
    ]

    SubFolders = [SubFolders[_] for _ in Exps]
    Freqs = [Freqs[_] for _ in Exps]

    Parameters = dict(
        ABRCh = Info['DAqs']['RecCh'],
        TTLCh = Info['DAqs']['TTLCh'],
        TimeWindow = [-0.003, 0.012],
        FilterFreq = [600, 1500]
    )

    ABRs.Session(SubFolders, Freqs, Intensities, **Parameters)
    Log['Done'].append((F,Folder))



#%% [ABRs] Get traces, waves amp and lat =======================================
Std = 1

ABRsExpEpochs = IO.Txt.Read(f"{AnalysisSharedPath}/GroupData/ABRs/ABRsExpEpochs.dict")
ABRsExpEpochs = IO.Txt.DictListsToArrays(ABRsExpEpochs)

#== Group data
print(f"[{ScriptName}] Getting waves amps and lats...")
for Group in Groups:
    FolderExps = Epochs[:2]
    # Get only ABR folders from specified group
    ExpMask = np.array([
            _.split('-')[-1] == 'ABRs'
            and _.split('/')[-1].split('-')[1].split('_')[0] == Group
        for _ in ABRsExpEpochs['Folders']
    ])
    ExpMask = {Exp: (ABRsExpEpochs['Exps'] == Exp)*ExpMask for Exp in FolderExps}
    Folders = {Exp: ABRsExpEpochs['Folders'][ExpMask[Exp]] for Exp in FolderExps}
    Animals = {Exp: ABRsExpEpochs['Animals'][ExpMask[Exp]] for Exp in FolderExps}

    ExpMask = {K: np.ones(len(V), dtype='bool') for K,V in Folders.items()}
    for E,Exp in enumerate(FolderExps):
        for F,Folder in enumerate(Folders[Exp]):
            # Get only animals with recordings for both exps
            if Animals[Exp][F] not in Animals[FolderExps[E-1]]:
                ExpMask[Exp][F] = False
                continue

            # Get only the last recording of each animal
            elif len(np.where((Animals[Exp] == Animals[Exp][F]))[0]) > 1:
                AnimalFolders = Folders[Exp][Animals[Exp] == Animals[Exp][F]]
                Dates = [int(_.split('/')[-1].split('-')[0]) for _ in AnimalFolders]
                if Folder != AnimalFolders[np.argmax(Dates)]:
                    ExpMask[Exp][F] = False

    Folders = {K: sorted(V[ExpMask[K]]) for K,V in Folders.items()}


    # Thresholds
    Thresholds = {
        E: ABRs.GetThresholdsPerFreq([AnalysisPath+'/'+_ for _ in Exp], Std=Std)
        for E,Exp in Folders.items()
    }

    Freqs = np.unique(np.concatenate((Thresholds['BeforeANT']['Freqs'], Thresholds['AfterANT']['Freqs'])))
    Freqs = sorted(Freqs, key=lambda i: int(i.split('-')[1]))
    for F in ['8000-16000', '8000-18000', '16000-18000']:
        if F in Freqs: Freqs.remove(F)
    ThresholdsFreqs = np.array(Freqs)

    ThresholdsFlat = [[Thresholds[Exp]['Thresholds'][Thresholds[Exp]['Freqs'] == _] for _ in Freqs] for Exp in FolderExps]
    ThresholdsFlat = [[ThresholdsFlat[0][_], ThresholdsFlat[1][_]] for _ in range(len(Freqs))]
    ThresholdsFlat = [_ for a in ThresholdsFlat for _ in a]


    # WaveAmps
    WaveAmps = {
        E: ABRs.GetWaveAmpPerFreq([AnalysisPath+'/'+_ for _ in Exp], Std=Std)
        for E,Exp in Folders.items()
    }

    Freqs = np.unique(np.concatenate((WaveAmps['BeforeANT']['Freqs'], WaveAmps['AfterANT']['Freqs'])))
    Freqs = sorted(Freqs, key=lambda i: int(i.split('-')[1]))
    for F in ['8000-16000', '8000-18000', '16000-18000']:
        if F in Freqs: Freqs.remove(F)
    WaveAmpsFreqs = np.array(Freqs)

    WaveFlat = {}
    for K in ('Amps', 'Latencies'):
        Flat = [[WaveAmps[Exp][K][WaveAmps[Exp]['Freqs'] == _,:].T for _ in Freqs] for Exp in FolderExps]
        Flat = [[Flat[0][_], Flat[1][_]] for _ in range(len(Freqs))]
        Flat = np.hstack([_ for a in Flat for _ in a])
        WaveFlat[K] = Flat
    for K in ('Freqs', 'Intensity', 'Animal'):
        Flat = [[WaveAmps[Exp][K][WaveAmps[Exp]['Freqs'] == _].T for _ in Freqs] for Exp in FolderExps]
        Flat = [[Flat[0][_], Flat[1][_]] for _ in range(len(Freqs))]
        Flat = np.hstack([_ for a in Flat for _ in a])
        WaveFlat[K] = Flat

    Flat = [[[Exp]*WaveAmps[Exp]['Animal'][WaveAmps[Exp]['Freqs'] == _].T.shape[0] for _ in Freqs] for Exp in FolderExps]
    Flat = [[Flat[0][_], Flat[1][_]] for _ in range(len(Freqs))]
    Flat = np.hstack([_ for a in Flat for _ in a])
    WaveFlat['Epoch'] = Flat


    # Write
    IO.Bin.Write(WaveFlat, AnalysisSharedPath+'/GroupData/ABRs/'+Group+'-WaveAmpLat')


print(f"[{ScriptName}] Done.")



#%% [ABRs] Waves amp and lat stats =============================================
Sets = ('Amps', 'Latencies')
Ws = (0,4)

WaveSet = {_: IO.Bin.Read(f"{AnalysisSharedPath}/GroupData/ABRs/{_}-WaveAmpLat")[0] for _ in ['Control', 'Tinnitus']}

ain = np.hstack([WaveSet[_]['Intensity'] for _ in Groups])
i = ain=='80'
for S,Set in enumerate(Sets):
    ad = np.hstack([WaveSet[_][Set][Ws[S],:] for _ in Groups])[i]
    aep = np.hstack([WaveSet[_]['Epoch'] for _ in Groups])[i]
    afr = np.hstack([WaveSet[_]['Freqs'] for _ in Groups])[i]
    agr = np.array([_ for G in Groups for _ in [G]*WaveSet[G][Set].shape[1]])[i]
    aid = np.hstack([WaveSet[_]['Animal'] for _ in Groups])[i]

    Paired = [False]*3 if S else [True,True,False]
    FactorNames = ('Epoch','Freq','Group')
    Factors = (aep,afr,agr)

    AR = Stats.AnOVa(ad, Factors, aid, Paired, 'auto', FactorNames)
    for _ in ('ANOVA', "Mauchly's Test for Sphericity", 'Sphericity Corrections'):
        if _ in AR.keys(): del(AR[_])

    AR['Full'] = {
        FactorNames[F]: Stats.AnOVa(ad, [Fac], aid, [Paired[F]], 'auto', [FactorNames[F]])
        for F,Fac in enumerate(Factors)
    }

    IO.Txt.Write(AR, f"{AnalysisSharedPath}/GroupData/ABRs/Stats-Wave{Ws[S]+1}{Set}-{'_'.join(Facs)}.dict")



#%% [ABRs] Threshold stats =====================================================
Scatter = {}
for Group in ['Control', 'Tinnitus']:
    Scatter[Group] = glob(AnalysisSharedPath+'/GroupData/ABRs/'+Group+'-Scatter/Threshold*.dat')
    Scatter[Group] = sorted(Scatter[Group], key=lambda x: int(x.split('/')[-1].split('_')[-1].split('.')[0]))
    Scatter[Group] = [IO.Bin.Read(Sc)[0] for Sc in Scatter[Group]]

Freqs = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ABRs/{Group}-Scatter/Freqs-Thresholds.dat')[0]

ThrsCtr = Scatter['Control']
ThrsTinn = Scatter['Tinnitus']

adc = [_ for a in ThrsCtr for _ in a]
adt = [_ for a in ThrsTinn for _ in a]
ad = np.array(adc+adt)
agr = np.array((['Control']*len(adc))+(['Tinnitus']*len(adt)))
afrc = np.repeat(Freqs,len(ThrsCtr[0])*2)
afrt = np.repeat(Freqs,len(ThrsTinn[0])*2)
afr = np.concatenate((afrc,afrt))
aepc = np.tile(np.repeat(['BeforeANT','AfterANT'],len(ThrsCtr[0])), len(ThrsCtr)//2)
aept = np.tile(np.repeat(['BeforeANT','AfterANT'],len(ThrsTinn[0])), len(ThrsTinn)//2)
aep = np.concatenate((aepc,aept))
aidc = np.tile(np.arange(len(ThrsCtr[0])),len(ThrsCtr))
aidt = np.tile(np.arange(len(ThrsTinn[0]))+1000,len(ThrsTinn))
aid = np.concatenate((aidc,aidt))

AR = Stats.AnOVa(ad, [agr,aep,afr], aid, [False,True,True],'auto',['Group','Epoch','Freq'])
IO.Txt.Write(AR, f"{AnalysisSharedPath}/GroupData/ABRs/Stats-Thr-Group_Epoch_Freq.dict")



#%% [ABRs] Threshold diff stats ================================================
Scatter = {}
for Group in ['Control', 'Tinnitus']:
    Scatter[Group] = glob(AnalysisSharedPath+'/GroupData/ABRs/'+Group+'-Scatter/Threshold*.dat')
    Scatter[Group] = sorted(Scatter[Group], key=lambda x: int(x.split('/')[-1].split('_')[-1].split('.')[0]))
    Scatter[Group] = [IO.Bin.Read(Sc)[0] for Sc in Scatter[Group]]

Freqs = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ABRs/{Group}-Scatter/Freqs-Thresholds.dat')[0]

ThrsCtr = Scatter['Control']
ThrsTinn = Scatter['Tinnitus']
ThrsCtr = abs(np.diff(ThrsCtr,axis=0))[::2,:]
ThrsTinn = abs(np.diff(ThrsTinn,axis=0))[::2,:]

adc = [_ for a in ThrsCtr for _ in a]
adt = [_ for a in ThrsTinn for _ in a]
ad = np.array(adc+adt)
agr = np.array((['Control']*len(adc))+(['Tinnitus']*len(adt)))
afrc = np.repeat(Freqs,len(ThrsCtr[0]))
afrt = np.repeat(Freqs,len(ThrsTinn[0]))
afr = np.concatenate((afrc,afrt))
aidc = np.tile(np.arange(len(ThrsCtr[0])),len(ThrsCtr))
aidt = np.tile(np.arange(len(ThrsTinn[0]))+1000,len(ThrsTinn))
aid = np.concatenate((aidc,aidt))

AR = Stats.AnOVa(ad, [agr,afr], aid, [False,True],'auto',['Group','Freq'])
IO.Txt.Write(AR, f"{AnalysisSharedPath}/GroupData/ABRs/Stats-ThrDiff-Group_Freq.dict")



#%% [ERPs] Detect and measure ERP peaks, latencies and ratios ==================
FilterFreq = [3,60]
FilterType = 'bandpass'
ERPWindow = [-0.5, 1]
BLDur = 0.2

(Folders, Animals, Treatments, Channels,
        ChannelsInvert, DoubletsAnimals, DoubletsTreatments,
        DoubletInRec1st, DoubletInCh1st, DoubletInRec2nd,
        DoubletInCh2nd, DoubletsTreatments, DoubletsAnimals
    ) = (
    IO.Bin.Read(f"{AnalysisFolder}/GroupData/ERPsInfoM/{K}")[0]
    for K in (
        'Folders', 'Animals', 'Treatments', 'Channels',
        'ChannelsInvert', 'DoubletsAnimals', 'DoubletsTreatments',
        'DoubletInRec1st', 'DoubletInCh1st', 'DoubletInRec2nd',
        'DoubletInCh2nd', 'DoubletsTreatments', 'DoubletsAnimals'
    )
)


# ========== Get raw traces
DataChannels, DataChannelsMean, ERPMeans, Rates, Animals = [], [], [], [], []
for F,Folder in enumerate(Folders):
    Channel = Channels[F]-1
    ChStart = Channel-4 if Channel>3 else 0
    DictFile = glob('/'.join(Folder.split('/')[:-1]) + '/*dict')[0]

    Data, Rate = ERPs.Load(Folder, InfoFile=DictFile)

    TTLs = ERPs.GetTTLs(Data[:,-1], Folder)
    ERP, X = ERPs.PairedERP(Data[:,:-2], Rate, TTLs, FilterFreq, FilterType, ERPWindow)
    ERPMean = ERP.mean(axis=1)
    Norm = ERPs.Analysis.Normalize(ERPMean, (-1, 1))

    ChInv = -1 if ChannelsInvert[F] else 1
    DataChannels[:,F] = Norm[:,Channel]*ChInv
    DataChannelsMean[:,F] = Norm[:,ChStart:Channel+1].mean(axis=1)
    ERPMeans[:,:,F] = ERPMean
    ERPRaw[:,:,:,F] = ERP
    Animals.append(Folder.split('/')[-2].split('-')[1])
    Rates.append(Rate)

ERPsTraces = {
    'DataChannels': DataChannels,
    'DataChannelsMean': DataChannelsMean,
    'Animals': Animals,
    'Rates': Rates,
    'ERPMeans': ERPMeans,
    'X': X
}

for K in ['Animals', 'Rates']: ERPsTraces[K] = np.array(ERPsTraces[K])

IO.Bin.Write(ERPsTraces, AnalysisSharedPath+'/GroupData/ERPsTraces')


# ========== Get peaks
Keys = [['P20', 'N40', 'P80', 'P20Ratios', 'N40Ratios', 'P80Ratios', 'Doublets'], []]
Keys[1] = [_+'Mean' for _ in Keys[0]]
ERPsPeaks = {K: [] for K in [b for a in Keys for b in a]}

for DCFolder in range(DataChannels.shape[1]):
    Norm, NormMean, Rate = DataChannels[:,DCFolder], DataChannelsMean[:,DCFolder], Rates[DCFolder]
    P20Time = int(round((0.02-X[0])*Rate))
    P520Time = int(round((0.52-X[0])*Rate))
    PWindows = [[P20Time-Rate*0.02, P20Time+Rate*0.1],
                [P520Time-Rate*0.02, P520Time+Rate*0.1]]

    for N,NormCh in enumerate([Norm, NormMean]):
        # Detect all peaks
        P = ERPs.Analysis.GetPeaks(NormCh, Std=0)
        P = sorted(np.concatenate((P['Pos'], P['Neg'])))
        P = [[_ for _ in P if _ in np.arange(W[0], W[1], dtype=int)] for W in PWindows]
        PDiff = [np.diff(NormCh[_]) for _ in P]
        PDiffSorted = [sorted(_) for _ in PDiff]

        # Detect N40 and P80 by negative-positive change
        PDiffN40 = [np.where((PDiff[_] == PDiffSorted[_][-1]))[0][0] for _ in range(2)]
        PDiffN40[1] += len(P[0])
        P = [b for a in P for b in a]
        PDiff = np.diff(NormCh[P])
        PDiffSorted = sorted(PDiff)

        PDiffN40 = sorted(PDiffN40)
        N40, P80 = [sorted([P[PDiffN40[0]+_], P[PDiffN40[1]+_]]) for _ in range(2)]

        # Detect P20 by positive-negative change happening before N40
        PDiffP20 = [1000, 1000]
        for Peak in range(2):
            Index = -1
            while PDiffP20[Peak] > PDiffN40[Peak] and Index < len(PDiffSorted)-1:
                Index += 1
                p20 = np.where((PDiff == PDiffSorted[Index]))[0][0]
                if PWindows[Peak][0] <= P[p20] <= PWindows[Peak][1]:
                    PDiffP20[Peak] = p20

        if 1000 in PDiffP20: print('Oh no!', DCFolder)

        P20 = sorted([P[PDiffP20[0]], P[PDiffP20[1]]])
        P20 = [v if NormCh[P[PDiffP20[i]+1]] < v else P[PDiffP20[i]+1] for i,v in enumerate(P20)]

        # Detect doublets and correct N40 to get the first doublet peak
        N40 = [P[P.index(P20[i])+1] if v != P20[i] else v for i,v in enumerate(N40)]
        Doublets = [P.index(P80[_])-P.index(N40[_])>1 for _ in range(2)]
        N40 = [np.where((NormCh == NormCh[np.arange(PWindows[_][0], PWindows[_][1], dtype=int)].min()))[0][-1] for _ in range(2)]

        # Correct P80 for the most positive point from N40-max(PWindow)
        if DCFolder not in [35]:
            for p,peak in enumerate(N40):
                P80[p] = NormCh[peak:int(PWindows[p][-1])].argmax()+peak

        ERPsPeaks[Keys[N][0]].append(P20)
        ERPsPeaks[Keys[N][1]].append(N40)
        ERPsPeaks[Keys[N][2]].append(P80)
        ERPsPeaks[Keys[N][6]].append(Doublets)

        # Amplitude ratio
        Zeros = [np.where((X >= 0))[0][0], np.where((X >= 0.5))[0][0]]
        BL = NormCh[Zeros[0]-int(Rate*BLDur):Zeros[0]].mean()

        for p,peak in enumerate(Keys[N][:3]):
            PeakValues = [NormCh[ERPsPeaks[Keys[N][p]][-1][0]]-BL, NormCh[ERPsPeaks[Keys[N][p]][-1][1]]-BL]
            Ratio = ((PeakValues[1]/PeakValues[0])-1)*100
            ERPsPeaks[Keys[N][p+3]].append(Ratio)


ERPsPeaks = {K: np.array(V) for K,V in ERPsPeaks.items()}
IO.Bin.Write(ERPsPeaks, AnalysisSharedPath+'/GroupData/ERPsPeaks')


# ========== Extract peak amplitudes, latencies and ratios
KeyPeaks = ('P20', 'N40', 'P80')
KeyData = 'DataChannels'

AnimalCtr = ERPsTraces['Animals'] == 'Control_06'
AnimalTinn = ERPsTraces['Animals'] == 'Tinnitus_01'
Labels = Treatments[AnimalCtr]
Rate = ERPsTraces['Rates'][AnimalCtr][0]
X = ERPsTraces['X']
P20 = int(round((0.02-X[0]) * Rate))
P520 = int(round((0.52-X[0]) * Rate))
N40 = int(round((0.04-X[0]) * Rate))
N540 = int(round((0.54-X[0]) * Rate))
P20Window = np.array([0.0, 0.03])
N40Window = np.array([0.01, 0.06])
P80Window = np.array([0.03, 0.1])
PWindows = [
    [P20-Rate*0.02, P20+Rate*0.1],
    [P520-Rate*0.02, P520+Rate*0.1]
]

KeyRatio = [_+'Ratios' for _ in KeyPeaks]
Window = [globals()[_+'Window'] for _ in KeyPeaks]

ControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in ERPsTraces['Animals']]
)

DoubletsControlAnimals = np.array(
    [True if 'Control' in _ else False for _ in DoubletsAnimals]
)

ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [], [], [], []
for K, Key in enumerate(KeyPeaks):
    for Tr,Treatment in enumerate(Labels):
        CR = ERPsPeaks[KeyRatio[K]][(Treatments == Treatment)*ControlAnimals]
        TR = ERPsPeaks[KeyRatio[K]][(Treatments == Treatment)*~ControlAnimals]
        CL = ERPsPeaks[Key][(Treatments == Treatment)*ControlAnimals]
        TL = ERPsPeaks[Key][(Treatments == Treatment)*~ControlAnimals]

        CL = (CL[:,1]+(Rate*X[0])-(Rate*0.5))/(CL[:,0]+(Rate*X[0]))
        CL = (CL-1)*100
        TL = (TL[:,1]+(Rate*X[0])-(Rate*0.5))/(TL[:,0]+(Rate*X[0]))
        TL = (TL-1)*100

        if not len(ControlRatios):
            ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
                np.zeros((CR.shape[0], len(Labels), len(KeyPeaks)), dtype=CR.dtype),
                np.zeros((TR.shape[0], len(Labels), len(KeyPeaks)), dtype=TR.dtype),
                np.zeros((CL.shape[0], len(Labels), len(KeyPeaks)), dtype=CL.dtype),
                np.zeros((TL.shape[0], len(Labels), len(KeyPeaks)), dtype=TL.dtype)
            ]

        (ControlRatios[:,Tr,K], TinnitusRatios[:,Tr,K],
            ControlLatencies[:,Tr,K], TinnitusLatencies[:,Tr,K]) = [
            CR, TR, CL, TL
        ]


PeakWidth = (ERPsPeaks['P80'] - ERPsPeaks['N40'])*1000/Rate
PeakAmps = np.zeros((2,len(ERPsTraces['Animals']), len(KeyPeaks)), dtype=ERPsTraces['DataChannels'].dtype)
PeakLatencies = np.zeros((2,len(ERPsTraces['Animals']),len(KeyPeaks)), dtype=PeakAmps.dtype)

for Tr,Treatment in enumerate(Treatments):
    Ch = Channels[Tr]-1
    Trace = ERPsTraces['ERPMeans'][:,Ch,Tr]

    for K,Key in enumerate(KeyPeaks):
        PeakAmps[:,Tr,K] = Trace[ERPsPeaks[Key][Tr,:]]

        PeakLatencies[:,Tr,K] = [
            ERPsPeaks[Key][Tr,0]/Rate+X[0],
            ERPsPeaks[Key][Tr,1]/Rate+X[0]-0.5
        ]

PeakLatencies *= 1000

IO.Bin.Write(
    {
        'ControlRatios': ControlRatios,
        'TinnitusRatios': TinnitusRatios,
        'ControlLatencies': ControlLatencies,
        'TinnitusLatencies': TinnitusLatencies,
        'Peaks': np.array(KeyPeaks)
    },
    AnalysisSharedPath+'/GroupData/ERPsRatiosLatencies'
)

IO.Bin.Write(PeakAmps, AnalysisSharedPath+'/GroupData/ERPsPeakAmps.dat')
IO.Bin.Write(PeakLatencies, AnalysisSharedPath+'/GroupData/ERPsPeakLatencies.dat')
IO.Bin.Write(PeakWidth, AnalysisSharedPath+'/GroupData/ERPsPeakWidth.dat')
IO.Bin.Write(Treatments, AnalysisSharedPath+'/GroupData/ERPsTreatments.dat')



#%% [ERPs] Stats ===============================================================
TreatmentsOrder = ('NaCl', 'Nic', 'Cann', 'Cann+Nic')
FacOrder = ('Group','Treatment','Click')
AOut = f'{AnalysisSharedPath}/GroupAnalysis/Stats'

Keys = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsRatiosLatencies/Peaks.dat')[0]
PeakAmps = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsPeakAmps.dat')[0]
PeakLats = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsPeakLatencies.dat')[0]
PeakWidths = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsPeakWidth.dat')[0]
Animals = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsInfo/Animals.dat')[0]
Treatments = IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsInfo/Treatments.dat')[0]

ControlRatios, TinnitusRatios, ControlLatencies, TinnitusLatencies = [
    IO.Bin.Read(f'{AnalysisSharedPath}/GroupData/ERPsRatiosLatencies/{K}.dat')[0]
    for K in ['ControlRatios', 'TinnitusRatios', 'ControlLatencies', 'TinnitusLatencies']
]

RatioAmps = np.array([
    np.concatenate((ControlRatios[:,:,K].T.ravel(), TinnitusRatios[:,:,K].T.ravel()))
    for K in range(len(Keys))
]).T

RatioLats = np.array([
    np.concatenate((ControlLatencies[:,:,K].T.ravel(), TinnitusLatencies[:,:,K].T.ravel()))
    for K in range(len(Keys))
]).T


print(f'[{ScriptName}] Processing Amps and Lats...')
aft = np.tile(Treatments,2)
afg = np.tile(Groups,2)
afc = np.array((['1']*Groups.shape[0])+(['2']*Groups.shape[0]))
aid = np.tile(Animals,2)
for K,Key in enumerate(Keys):
    print(f'[{ScriptName}]     {Key}...')

    for D,Data in (('Amp', PeakAmps),('Lat',PeakLats)):
        print(f'[{ScriptName}]         {D}...')
        ad = Data[:,:,K].ravel()
        A = Stats.AnOVa(
            ad, [afg,aft,afc], aid, [False,True,True], 'auto',
            FacOrder, GetAllPairwise=True, GetInvalidPWCs=True
        )
        IO.Txt.Write(A, f'{AOut}/ERPs-{D}-Group_Treatment_Click-{Key}.dict')

        A = Stats.AnOVa(ad, [afg], [aid], [False], 'auto', ('Group',))
        IO.Txt.Write(A, f'{AOut}/ERPs-{D}-Group-{Key}.dict')


print(f'[{ScriptName}] Processing Widths...')
ad = PeakWidths.ravel()
A = Stats.AnOVa(
    ad, [afg,aft,afc], aid, [False,True,True], 'auto',
    FacOrder, GetAllPairwise=True, GetInvalidPWCs=True
)
AR = Stats.GetAnovaReport(A, FacOrder)

IO.Txt.Write(A, f'{AnalysisSharedPath}/GroupAnalysis/Stats/ERPs-Width-Group_Treatment_Click.dict')

with open(f'{AnalysisSharedPath}/GroupAnalysis/Stats/ERPs-Width-Group_Treatment_Click-Report.dict', 'w') as f:
    f.writelines(AR)


print(f'[{ScriptName}] Processing Ratios...')
FacOrder = ('Group','Treatment')
afg = Groups
aft = Treatments
aid = Animals
for K,Key in enumerate(Keys):
    print(f'[{ScriptName}]     {Key}...')

    for D,Data in (('Amp', RatioAmps),('Lat',RatioLats)):
        print(f'[{ScriptName}]         {D}...')
        ad = Data[:,K]
        A = Stats.AnOVa(
            ad, [afg,aft], aid, [False,True], 'auto',
            FacOrder, GetAllPairwise=True, GetInvalidPWCs=True
        )
        AR = Stats.GetAnovaReport(A, FacOrder)

        IO.Txt.Write(A, f'{AnalysisSharedPath}/GroupAnalysis/Stats/ERPs-Ratio{D}-Group_Treatment-{Key}.dict')

        with open(f'{AnalysisSharedPath}/GroupAnalysis/Stats/ERPs-Ratio{D}-Group_Treatment-{Key}-Report.dict', 'w') as f:
            f.writelines(AR)

print(f'[{ScriptName}] Done.')



#%% [ERPs] Stats doublets ======================================================
SDPairs = (
    (ERPsPeaks['Doublets'][ControlAnimals].ravel(), ERPsPeaks['Doublets'][~ControlAnimals].ravel()),
    (ERPsPeaks['Doublets'][ControlAnimals,0], ERPsPeaks['Doublets'][~ControlAnimals,0]),
    (ERPsPeaks['Doublets'][ControlAnimals,1], ERPsPeaks['Doublets'][~ControlAnimals,1]),
)

SDPairsNames = (
    'Group',
    'Group-C1',
    'Group-C2',
)

Res = []
for P,Pair in enumerate(SDPairs):
    RGroup = pd.crosstab(Pair[0], Pair[1])
    RGroup = mcnemar(RGroup.to_numpy())
    Res.append((RGroup.statistic, RGroup.pvalue))

Res = np.array(Res)

IO.Bin.Write(Res, AnalysisSharedPath+'/GroupData/Stats-ERPsDoublets-Group_Click.dat')



#%% [GPIAS] Batch ==============================================================
ParametersAnalysis = dict(
    TimeWindow = [-0.2, 0.2],
    SliceSize = 0.2,
    FilterFreq = [100],
    FilterOrder = 2,
    FilterType = 'lowpass',
    Filter = 'butter',
    Return = True,
)

Stim = 'Sound'

for Group in Groups:
    Exps = sorted(glob(DataPath + '/' + Group+'/2*IAS'))

    GPIASIndexes = {}

    print(f'[{ScriptName}] Running analysis for group {Group}...')
    for Exp in Exps:
        Folders = sorted(glob(Exp + '/' + Exp.split('/')[-1][:4] + '-*'))
        Files = sorted(glob(Exp + '/' + Exp.split('/')[-1][:4] + '*dict'))

        StimExps = []
        for F, Folder in enumerate(Folders):
            DataInfo = IO.Txt.Read(Files[F])

            if 'ExpInfo' not in DataInfo:
                DataInfo = IO.Txt.Dict_OldToNew(DataInfo)
                IO.Txt.Write(DataInfo, Files[F])

            if not DataInfo['ExpInfo']:
                DataInfo = IO.Txt.Dict_OldToNew(DataInfo)
                IO.Txt.Write(DataInfo, Files[F])

            if Stim in DataInfo['Animal']['StimType']: StimExps.append(Folder)

        StimExps.sort()
        for F, Folder in enumerate(StimExps):
            print(f"[{ScriptName}]     {Folder.replace('/'.join(DataPath,Group)+'/','')}")

            if len(Folder.split('_')[-2]) > 2:
                Animal = '_'.join(Folder.split('_')[-2:])
            else:
                Animal = Folder.split('_')[-1]

            if Animal not in GPIASIndexes: GPIASIndexes[Animal] = {}

            RecFolder = Folder.split('/')[-1]


            print(f"[{ScriptName}]         Loading data...")
            Data, Rate = IO.DataLoader(Folder, Unit='mV')
            Proc = list(Data.keys())[0]
            DataExp = list(Data[Proc].keys())[0]
            Data, Rate = Data[Proc][DataExp], Rate[Proc][DataExp]

            ExpStim = '_'.join(DataInfo['Animal']['StimType'])

            RecFolder = Folder.split('/')[-1]
            SavePath = Exp.replace(DataPath, AnalysisPath)
            SavePath += f"/{Files[F][:-5].split('/')[-1]}-{ExpStim}"
            DataInfo = IO.Txt.Read(Files[F])

            Animal = DataInfo['Animal']['AnimalName']
            if Animal not in GPIASIndexes: GPIASIndexes[Animal] = {}


            print(f"[{ScriptName}]         Getting all trials...")
            GPIASAll = GPIAS.GetAllTrials(
                Data, Rate, DataInfo, **ParametersAnalysis
            )
            GPIASAll = GPIASAll['IndexTrace']

            Freqs = GPIASAll.keys()
            for Fr in Freqs:
                GPIASAll[Fr]['Gap'] = np.array(GPIASAll[Fr]['Gap']).T
                GPIASAll[Fr]['NoGap'] = np.array(GPIASAll[Fr]['NoGap']).T

            IO.Bin.Write(GPIASAll, SavePath+'/GPIASAllTrials')


            print(f"[{ScriptName}]         Getting all indexes...")
            GPIASRec, X = GPIAS.Analysis(
                Data, Rate, DataInfo, Save=SavePath, **ParametersAnalysis
            )

            GPIASIndexes[Animal].update({
                F: Freq['GPIASIndex']
                for F, Freq in GPIASRec['Index'].items()
            })

            del(GPIASRec, GPIASAll, Data, X)
            gc.collect()

            print(f"[{ScriptName}]         Done.")

print('[{ScriptName}] All done.')



#%% [GPIAS] DataSet ============================================================
EpochsOrder = ('BeforeANT', 'AfterANT')
ExpEpochs = IO.Txt.Read(f"{AnalysisSharedPath}/GroupData/GPIAS/GPIASExpEpochs.dict")
ExpEpochs = IO.Txt.DictListsToArrays(ExpEpochs)

for Group in Groups:
    Exps = {
        E: [
            _ for _ in sorted(glob(
                f'{AnalysisPath}/{Group}/*GPIAS/*'
            ))
            if _.split('/')[-2].split('-')[0] in Epoch
        ]
        for E,Epoch in Epochs.items()
    }


    print(f'[{ScriptName}] Calculating...')
    Indexes_IFAE, Traces_IFAE, X = GPIAS.GetExpsIndexesDict(Exps)
    GPIASGroup = {}
    GPIASGroup['Animals'] = np.unique(Indexes_IFAE['Animals'])
    GPIASGroup['Freqs'] = sorted(np.unique(Indexes_IFAE['Freqs']), key=lambda x: int(x.split('-')[1]))
    GPIASGroup['MAF'] = GPIAS.GetMAF(Indexes_IFAE, GPIASGroup['Animals'], EpochsOrder)
    GPIASGroup['Indexes'] = GPIAS.GetMAFIndexes(Indexes_IFAE, GPIASGroup['MAF'], GPIASGroup['Animals'], Epochs)


    print(f'[{ScriptName}] Writing results to {AnalysisSharedPath}...')
    if 'Tinnitus_05' in Traces_IFAE['Animals']:
        Index = (Traces_IFAE['Animals'] == 'Tinnitus_05')*(Traces_IFAE['Freqs'] == '8000-10000')
        IO.Bin.Write(
            {
                **{
                    K: np.array([V[_] for _ in np.where(Index)[0]])
                    for K,V in Traces_IFAE.items()
                    if K != 'Dimensions'
                },
                **{'X': X},
                **{'Dimensions': np.concatenate((
                    ['Trial'],Traces_IFAE['Dimensions']
                ))}
            },
            f'{AnalysisSharedPath}/GroupData/GPIAS/{Group}-RawTraces'
        )

    IO.Txt.Write(GPIASGroup, f'{AnalysisSharedPath}/GroupData/GPIAS/{Group}-GPIASIndexes.dict')
    IO.Txt.Write(Indexes_IFAE, f'{AnalysisSharedPath}/GroupData/GPIAS/{Group}-Indexes_IFAE.dict')
    IO.Bin.Write(Traces_IFAE, f'{AnalysisSharedPath}/GroupData/GPIAS/{Group}-Traces_IFAE')


    print(f'[{ScriptName}] Getting indexes for all freqs...')
    Animals, Freqs = (GPIASGroup[_] for _ in ('Animals', 'Freqs'))
    DataPerFreq = np.zeros((len(Animals), len(Freqs), len(Exps)), dtype=Indexes_IFAE['Index'].dtype)
    for E,Exp in enumerate(Exps):
        for F,Freq in enumerate(Freqs):
            for A,Animal in enumerate(Animals):
                EFA = (
                    Indexes_IFAE['Freqs'] == Freq)*(
                    Indexes_IFAE['Exps'] == Exp)*(
                    Indexes_IFAE['Animals'] == Animal
                )
                DataPerFreq[A,F,E] = Indexes_IFAE['Index'][EFA] if True in EFA else np.nan

    for E in range(DataPerFreq.shape[2]):
        for F in range(DataPerFreq.shape[1]):
            NaNs = np.isnan(DataPerFreq[:,F,E])
            DataPerFreq[NaNs,F,E] = np.nanmean(DataPerFreq[:,F,E])

    All = [[DataPerFreq[:,_,0], DataPerFreq[:,_,1]] for _ in range(DataPerFreq.shape[1])]
    All = abs(np.array([_ for b in All for _ in b]))
    IO.Bin.Write(All, f'{AnalysisSharedPath}/GroupData/GPIAS/{Group}-AllFreqs.dat')

    print(f'[{ScriptName}] Done.')



#%% [GPIAS] All freq statistics ================================================
EpochsOrder = ('BeforeANT', 'AfterANT')
ExpEpochs = IO.Txt.Read(f"{AnalysisSharedPath}/GroupData/GPIAS/GPIASExpEpochs.dict")
ExpEpochs = IO.Txt.DictListsToArrays(ExpEpochs)

adFull, afrFull, aepFull, agrFull, aidFull = [[] for _ in range(5)]

for Group in Groups:
    print(Group)
    Indexes_IFAE = IO.Txt.Read(f"{AnalysisSharedPath}/GroupData/GPIAS/{Group}-Indexes_IFAE.dict")
    Indexes_IFAE = IO.Txt.DictListsToArrays(Indexes_IFAE)

    Animals = np.unique(Indexes_IFAE['Animals'])
    Freqs = sorted(np.unique(Indexes_IFAE['Freqs']), key=lambda x: int(x.split('-')[1]))

    ad, afr, aep, aid = [[] for _ in range(4)]
    for E,Exp in enumerate(Epochs):
        for F,Freq in enumerate(Freqs):
            for A,Animal in enumerate(Animals):
                EFA = (
                    (Indexes_IFAE['Freqs'] == Freq) *
                    (Indexes_IFAE['Exps'] == Exp) *
                    (Indexes_IFAE['Animals'] == Animal)
                )
                ad.append(Indexes_IFAE['Index'][EFA][0][0] if True in EFA else np.nan)
                afr.append(Freq)
                aep.append(Exp)
                aid.append(Animal)

    ad, afr, aep, aid = (
        np.array(_)
        for _ in (ad, afr, aep, aid)
    )

    for p,f in ((ad,adFull),(afr,afrFull),(aep,aepFull),(aid,aidFull)):
        f.append(p)

    agrFull.append(np.array([Group]*len(ad)))

    AR = Stats.AnOVa(ad,[afr,aep],aid,[True,True],'auto',['Freq','Epoch'])
    IO.Txt.Write(
        AR,
        f'{AnalysisSharedPath}/GroupData/GPIAS/Stats-GI-{Group}-Epoch_Freq.dict'
    )

adFull, afrFull, aepFull, agrFull, aidFull = [
    np.concatenate(_)
    for _ in (adFull, afrFull, aepFull, agrFull, aidFull)
]

ARFull = Stats.AnOVa(adFull,[afrFull,aepFull,agrFull],aidFull,[True,True,False],'auto',['Freq','Epoch','Group'])
IO.Txt.Write(ARFull, f'{AnalysisSharedPath}/GroupData/GPIAS/Stats-GI-Epoch_Freq_Group.dict')

print('All done.')



#%% [GPIAS] MAF statistics =====================================================
ControlMAF = IO.Txt.Read(f'{AnalysisSharedPath}/GroupData/GPIAS/Control-GPIASIndexes.dict')
TinnitusMAF = IO.Txt.Read(f'{AnalysisSharedPath}/GroupData/GPIAS/Tinnitus-GPIASIndexes.dict')

ad = np.ravel(ControlMAF['Indexes']+TinnitusMAF['Indexes'])
agr = np.ravel([
    ['Control']*len(ControlMAF['Indexes'][0]),
    ['Control']*len(ControlMAF['Indexes'][1]),
    ['Tinnitus']*len(TinnitusMAF['Indexes'][0]),
    ['Tinnitus']*len(TinnitusMAF['Indexes'][1])
])
aep = np.ravel([
    ['Before']*len(ControlMAF['Indexes'][0]),
    ['After']*len(ControlMAF['Indexes'][1]),
    ['Before']*len(TinnitusMAF['Indexes'][0]),
    ['After']*len(TinnitusMAF['Indexes'][1])
])
aid = np.ravel([
    np.arange(len(ControlMAF['Indexes'][0])),
    np.arange(len(ControlMAF['Indexes'][1])),
    np.arange(len(TinnitusMAF['Indexes'][0]))+1000,
    np.arange(len(TinnitusMAF['Indexes'][1]))+1000,
])

AR = Stats.AnOVa(ad, [agr,aep], aid, [False,True], 'auto', ['Group','Epoch'])
IO.Txt.Write(AR, f'{AnalysisSharedPath}/GroupData/GPIAS/Stats-MAF-Group_Epoch.dict')



#%% EOF ========================================================================
